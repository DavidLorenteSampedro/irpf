page 77001 "MovimientosIRPF"
{
    //#region Movimientos IRPF
    Caption = 'Movimientos IRPF', Comment = 'ESP="Movimientos IRPF"';
    PageType = List;
    UsageCategory = Lists;
    ApplicationArea = All;
    SourceTable = MovimientosIRPF;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = false;
    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field(NoMovimientoIRPF; NoMovimientoIRPF)
                {
                    ApplicationArea = All;
                }
                field(NoDocumentoIRPF; NoDocumentoIRPF)
                {
                    ApplicationArea = All;
                }
                field(TipoDocumentoIRPF; TipoDocumentoIRPF)
                {
                    ApplicationArea = All;
                }
                field(FechaRegistroIRPF; FechaRegistroIRPF)
                {
                    ApplicationArea = All;
                }
                field(TipoIRPF; TipoIRPF)
                {
                    ApplicationArea = All;
                }
                field(NoProveedorClienteIRPF; NoProveedorClienteIRPF)
                {
                    ApplicationArea = All;
                }
                field(GrupoIRPF; GrupoIRPF)
                {
                    ApplicationArea = All;
                }
                field(TipoCalculo; TipoCalculo)
                {
                    ApplicationArea = All;
                }
                field(BaseIRPF; BaseIRPF)
                {
                    ApplicationArea = All;
                }
                field(PorcentajeRetencionIRPF; PorcentajeRetencionIRPF)
                {
                    ApplicationArea = All;
                }
                field(ImporteIRPF; ImporteIRPF)
                {
                    ApplicationArea = All;
                }
                field(PendienteIRPF; PendienteIRPF)
                {
                    ApplicationArea = All;
                }
                field(FechaLiquidacionIRPF; FechaLiquidacionIRPF)
                {
                    ApplicationArea = All;
                }
            }
        }
        area(Factboxes)
        {

        }
    }
    actions
    {
        area(Navigation)
        {
            action(NavigateDocument)
            {
                Promoted = true;
                ApplicationArea = All;
                Image = Navigate;
                Caption = 'Navigate', Comment = 'ESP="Navegar"';
                ToolTip = 'Navigate current mov.', Comment = 'ESP="Navega el movimiento actual"';
                trigger OnAction()
                var
                    Navigate: Page Navigate;
                begin
                    Navigate.SetDoc(FechaRegistroIRPF, NoDocumentoIRPF);
                    Navigate.Run();
                end;
            }
            action(NavigateLiqDocument)
            {
                Promoted = true;
                ApplicationArea = All;
                Image = Statistics;
                Caption = 'Navigate Liq. Document', Comment = 'ESP="Navegar Liquidación"';
                ToolTip = 'Navigate Liq. of current mov.', Comment = 'ESP="Navega la liquidación del movimiento actual"';
                trigger OnAction()
                var
                    Navigate: Page Navigate;
                begin
                    if Rec.PendienteIRPF = true then
                        Error('El movimiento no esta liquidado.');
                    Navigate.SetDoc(FechaLiquidacionIRPF, NoDocumentoLiquidacionIRPF);
                    Navigate.Run();
                end;
            }
        }
        area(Processing)
        {
            group(IRPF)
            {
                Caption = 'IRPF', Comment = 'ESP="IRPF"';
                action(LiquidarIRPF)
                {
                    ApplicationArea = All;
                    Image = AmountByPeriod;
                    Caption = 'Calcular y liquidar IRPF', Comment = 'ESP="Calcular y liquidar IRPF"';
                    RunObject = report LiquidarIRPF;
                }
            }
        }
    }
    trigger OnOpenPage()
    var
        SetupIRPF: Codeunit SetupIRPF;
    begin
        SetupIRPF.SetupJournalsTemplates();
    end;
    //#endregion
}