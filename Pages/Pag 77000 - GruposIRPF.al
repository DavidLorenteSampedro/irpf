page 77000 "GruposIRPF"
{
    //#region Grupos IRPF
    Caption = 'Grupos IRPF', Comment = 'ESP="Grupos IRPF"';
    PageType = List;
    UsageCategory = Lists;
    ApplicationArea = All;
    SourceTable = GruposIRPF;

    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field(CodigoIRPF; CodigoIRPF)
                {
                    ApplicationArea = All;
                }
                field(DescripcionIRPF; DescripcionIRPF)
                {
                    ApplicationArea = All;
                }
                field(TipoCalculoIRPF; TipoCalculoIRPF)
                {
                    ApplicationArea = All;
                }
                field(PorcentajeRetencionIRPF; PorcentajeRetencionIRPF)
                {
                    ApplicationArea = All;
                }
                field(CuentaRetencionSoportadoIRPF; CuentaRetencionSoportadoIRPF)
                {
                    ApplicationArea = All;
                }
                field(CuentaRetencionRepercutidoIRPF; CuentaRetencionRepercutidoIRPF)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            group(IRPF)
            {
                Caption = 'IRPF', Comment = 'ESP="IRPF"';
                action(LiquidarIRPF)
                {
                    ApplicationArea = All;
                    Image = AmountByPeriod;
                    Caption = 'Calcular y liquidar IRPF', Comment = 'ESP="Calcular y liquidar IRPF"';
                    RunObject = report LiquidarIRPF;
                }
            }
        }
    }
    trigger OnOpenPage()
    var
        SetupIRPF: Codeunit SetupIRPF;
    begin
        SetupIRPF.SetupJournalsTemplates();
    end;
    //#endregion
}