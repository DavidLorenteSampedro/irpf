enum 77002 "TipoDocumentoIRPF"
{
    Extensible = true;

    value(0; Factura)
    {
        Caption = 'Factura', Comment = 'ESP="Factura"';
    }
    value(1; Abono)
    {
        Caption = 'Abono', Comment = 'ESP="Abono"';
    }
}