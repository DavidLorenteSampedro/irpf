enum 77003 "TipoContrapartidaIRPF"
{
    Extensible = true;

    value(0; Blank)
    {
        Caption = ' ', Comment = 'ESP=" "';
    }
    value(1; Cuenta)
    {
        Caption = 'Cuenta', Comment = 'ESP="Cuenta"';
    }
    value(2; Banco)
    {
        Caption = 'Banco', Comment = 'ESP="Banco"';
    }
}