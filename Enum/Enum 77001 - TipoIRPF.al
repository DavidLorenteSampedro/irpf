enum 77001 "TipoIRPF"
{
    Extensible = true;

    value(0; Compras)
    {
        Caption = 'Compras', Comment = 'ESP="Compras"';
    }
    value(1; Ventas)
    {
        Caption = 'Ventas', Comment = 'ESP="Ventas"';
    }
}