enum 77000 "TipoCalculoIRPF"
{
    Extensible = true;

    value(0; Blank)
    {
        Caption = ' ', Comment = 'ESP=" "';
    }
    value(1; Importe)
    {
        Caption = 'Importe', Comment = 'ESP="Importe"';
    }
    value(2; ImporteIVAIncluido)
    {
        Caption = 'Importe IVA Incluido', Comment = 'ESP="Importe IVA Incluido"';
    }
}