tableextension 77004 "PurchaseHeaderIRPF" extends "Purchase Header" //38
{
    //#region Purchase Header
    fields
    {
        field(50000; CodigoGrupoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Código Grupo IRPF', Comment = 'ESP="Código Grupo IRPF"';
            TableRelation = GruposIRPF.CodigoIRPF;
            trigger OnValidate()
            var
                GruposIRPF: Record GruposIRPF;
                Vendor: Record Vendor;
            begin
                //Calculamos las lineas de nuevo
                if Rec.CodigoGrupoIRPF <> xRec.CodigoGrupoIRPF then
                    Rec.RecreatePurchLines(CopyStr(Rec.FieldCaption(CodigoGrupoIRPF), 1, 100));
                if Rec.CodigoGrupoIRPF <> '' then begin
                    Vendor.Get(Rec."Buy-from Vendor No.");
                    if Vendor.CodigoGrupoIRPF = '' then
                        Error('No se puede calcular IRPF si el proveedor no tiene configurado IRPF en su ficha.')
                    else
                        if GruposIRPF.Get(Rec.CodigoGrupoIRPF) then begin
                            Rec.TipoCalculoIRPF := GruposIRPF.TipoCalculoIRPF;
                            Rec.PorcentajeIRPF := GruposIRPF.PorcentajeRetencionIRPF;
                            Rec.CalcFields(BaseIRPF, ImporteIRPF);
                        end;
                end else begin
                    Rec.CodigoGrupoIRPF := '';
                    Rec.TipoCalculoIRPF := Rec.TipoCalculoIRPF::Blank;
                    Rec.PorcentajeIRPF := 0;
                end;
            end;
        }
        field(50001; ImporteIRPF; Decimal)
        {
            Caption = 'Importe IRPF', Comment = 'ESP="Importe IRPF"';
            Editable = false;
            FieldClass = FlowField;
            AutoFormatExpression = "Currency Code";
            CalcFormula = Sum ("Purchase Line".ImporteIRPF WHERE("Document Type" = FIELD("Document Type"), "Document No." = FIELD("No.")));
        }
        field(50002; TipoCalculoIRPF; Enum TipoCalculoIRPF)
        {
            DataClassification = CustomerContent;
            Caption = 'Tipo Calculo IRPF', Comment = 'ESP="Tipo Calculo IRPF"';
            Editable = false;
        }
        field(50003; BaseIRPF; Decimal)
        {
            Caption = 'Base IRPF', Comment = 'ESP="Base IRPF"';
            Editable = false;
            FieldClass = FlowField;
            AutoFormatExpression = "Currency Code";
            CalcFormula = Sum ("Purchase Line".BaseIRPF WHERE("Document Type" = FIELD("Document Type"), "Document No." = FIELD("No.")));
        }
        field(50004; PorcentajeIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Porcentaje IRPF', Comment = 'ESP="Porcentaje IRPF"';
            Editable = false;
            MinValue = 0;
            MaxValue = 100;
        }
    }
    //#endregion
}