tableextension 77014 "PurchInvHeaderIRPF" extends "Purch. Inv. Header" //122
{
    //#region Purch Inv Header
    fields
    {
        field(50000; CodigoGrupoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Código Grupo IRPF', Comment = 'ESP="Código Grupo IRPF"';
            Editable = false;
        }
        field(50001; ImporteIRPF; Decimal)
        {
            Caption = 'Importe IRPF', Comment = 'ESP="Importe IRPF"';
            Editable = false;
            FieldClass = FlowField;
            AutoFormatExpression = "Currency Code";
            CalcFormula = Sum ("Purch. Inv. Line".ImporteIRPF WHERE("Document No." = FIELD("No.")));
        }
        field(50002; TipoCalculoIRPF; Enum TipoCalculoIRPF)
        {
            DataClassification = CustomerContent;
            Caption = 'Tipo Calculo IRPF', Comment = 'ESP="Tipo Calculo IRPF"';
            Editable = false;
        }
        field(50003; BaseIRPF; Decimal)
        {
            Caption = 'Base IRPF', Comment = 'ESP="Base IRPF"';
            Editable = false;
            FieldClass = FlowField;
            AutoFormatExpression = "Currency Code";
            CalcFormula = Sum ("Purch. Inv. Line".BaseIRPF WHERE("Document No." = FIELD("No.")));
        }
        field(50004; PorcentajeIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Porcentaje IRPF', Comment = 'ESP="Porcentaje IRPF"';
            Editable = false;
        }
    }
    //#endregion
}