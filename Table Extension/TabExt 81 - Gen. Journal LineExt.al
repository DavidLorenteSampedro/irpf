tableextension 77006 "GenJournalLineIRPF" extends "Gen. Journal Line" //81
{
    fields
    {
        field(50000; CodigoGrupoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Código Grupo IRPF', Comment = 'ESP="Código Grupo IRPF"';
            TableRelation = GruposIRPF.CodigoIRPF;
        }
        field(50001; TipoCalculoIRPF; Enum TipoCalculoIRPF)
        {
            DataClassification = CustomerContent;
            Caption = 'Tipo Cálculo IRPF', Comment = 'ESP="Tipo Cálculo IRPF"';
            Editable = false;
        }
        field(50002; ImporteIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Importe IRPF', Comment = 'ESP="Importe IRPF"';
            AutoFormatExpression = "Currency Code";
        }
        field(50003; BaseIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Base IRPF', Comment = 'ESP="Base IRPF"';
            AutoFormatExpression = "Currency Code";
        }
        field(50004; ImporteDLIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Importe IRPF (DL)', Comment = 'ESP="Importe IRPF (DL)"';
        }
        field(50005; BaseDLIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Base IRPF (DL)', Comment = 'ESP="Base IRPF (DL)"';
        }
        field(50006; CodigoGrupoContrapartidaIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Código Grupo IRPF Contrapartida', Comment = 'ESP="Código Grupo IRPF Contrapartida"';
            TableRelation = GruposIRPF.CodigoIRPF;
        }
        field(50007; GuidMovimientoIRPF; Guid)
        {
            DataClassification = CustomerContent;
        }
    }
}