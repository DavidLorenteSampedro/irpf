tableextension 77023 "ReturnReceiptLineIRPF" extends "Return Receipt Line" //6661
{
    fields
    {
        field(50000; CodigoGrupoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Código Grupo IRPF', Comment = 'ESP="Código Grupo IRPF"';
            TableRelation = GruposIRPF.CodigoIRPF;
        }
        field(50001; TipoCalculoIRPF; Enum TipoCalculoIRPF)
        {
            DataClassification = CustomerContent;
            Caption = 'Tipo Cálculo IRPF', Comment = 'ESP="Tipo Cálculo IRPF"';
        }
        field(50002; ImporteIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Importe IRPF', Comment = 'ESP="Importe IRPF"';
        }
        field(50003; BaseIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Base IRPF', Comment = 'ESP="Base IRPF"';
        }
        field(50004; PorcentajeRetencionIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = '% Retención', Comment = 'ESP="% Retención"';
            MinValue = 0;
            MaxValue = 100;
        }
    }
}