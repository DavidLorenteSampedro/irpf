tableextension 77005 "PurchaseLineIRPF" extends "Purchase Line" //39
{
    //#region Purchase Line
    fields
    {
        field(50000; CodigoGrupoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Código Grupo IRPF', Comment = 'ESP="Código Grupo IRPF"';
            TableRelation = GruposIRPF.CodigoIRPF;
            Editable = false;
        }
        field(50001; TipoCalculoIRPF; Enum TipoCalculoIRPF)
        {
            DataClassification = CustomerContent;
            Caption = 'Tipo Cálculo IRPF', Comment = 'ESP="Tipo Cálculo IRPF"';
            Editable = false;
        }
        field(50002; ImporteIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Importe IRPF', Comment = 'ESP="Importe IRPF"';
            AutoFormatExpression = "Currency Code";
            Editable = false;
        }
        field(50003; BaseIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Base IRPF', Comment = 'ESP="Base IRPF"';
            AutoFormatExpression = "Currency Code";
            Editable = false;
        }
        field(50004; PorcentajeRetencionIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = '% Retención', Comment = 'ESP="% Retención"';
            MinValue = 0;
            MaxValue = 100;
            Editable = false;
        }
        field(50005; CalculaIRPF; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Calcula IRPF', Comment = 'ESP="Calcula IRPF"';
            InitValue = true;
        }
    }
    procedure GetTotalIRPF(): Decimal
    var
        TotalIRPF: Decimal;
    begin
        TotalIRPF := 0;
        IF Rec.Find('-') then
            repeat
                TotalIRPF := TotalIRPF + Rec.ImporteIRPF;
            until Rec.Next() = 0;
        EXIT(TotalIRPF);
    end;
    //#endregion
}