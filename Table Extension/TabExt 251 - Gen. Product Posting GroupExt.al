tableextension 77018 "GenProductPostingGroupIRPF" extends "Gen. Product Posting Group" //251
{
    fields
    {
        field(50000; SoportaIRPF; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Soporta IRPF', Comment = 'ESP="Soporta IRPF"';
        }
    }
}