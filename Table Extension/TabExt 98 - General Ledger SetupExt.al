tableextension 77007 "GeneralLedgerSetupIRPF" extends "General Ledger Setup" //98
{
    fields
    {
        field(50000; MaximaDiferenciaIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Máxima Diferencia IRPF', Comment = 'ESP="Máxima Diferencia IRPF"';
        }
    }
}