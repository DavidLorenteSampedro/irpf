tableextension 77020 "PurchasesPayablesSetupIRPF" extends "Purchases & Payables Setup" //312
{
    //#region Purchases & Payables Setup
    fields
    {
        field(50000; ActivoIRPF; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Activo', Comment = 'ESP="Activo"';
        }
        field(50001; MostrarTotalFacturaSinIRPF; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Mostrar Total Factura Sin IRPF', Comment = 'ESP="Mostrar Total Factura Sin IRPF"';
        }
        field(50002; AvisoSetup1IRPF; Boolean)
        {
            DataClassification = CustomerContent;
        }
        field(50003; AvisoSetup2IRPF; Boolean)
        {
            DataClassification = CustomerContent;
        }
        field(50004; AvisoSetup3IRPF; Boolean)
        {
            DataClassification = CustomerContent;
        }
        field(50005; LibroDiarioIRPF; Code[10])
        {
            DataClassification = CustomerContent;
            Caption = 'Journal Template Name', Comment = 'ESP="Nombre libro diario"';
            TableRelation = "Gen. Journal Template";
        }
        field(50006; SeccionDiarioIRPF; Code[10])
        {
            DataClassification = CustomerContent;
            Caption = 'Journal Batch Name', Comment = 'ESP="Nombre sección diario"';
            TableRelation = "Gen. Journal Batch".Name WHERE("Journal Template Name" = FIELD(LibroDiarioIRPF));
        }
    }
    //#endregion
}