tableextension 77001 "VendorIRPF" extends Vendor //23
{
    //#region Vendor
    fields
    {
        field(50000; CodigoGrupoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Código Grupo IRPF', Comment = 'ESP="Código Grupo IRPF"';
            TableRelation = GruposIRPF.CodigoIRPF;
        }
    }
    //#endregion
}