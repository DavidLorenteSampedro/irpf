tableextension 77000 "CustomerIRPF" extends Customer //18
{
    fields
    {
        field(50000; CodigoGrupoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Código Grupo IRPF', Comment = 'ESP="Código Grupo IRPF"';
            TableRelation = GruposIRPF.CodigoIRPF;
        }
    }
}