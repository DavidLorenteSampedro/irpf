tableextension 77019 "VATAmountLineIRPF" extends "VAT Amount Line" //290
{
    //#region VAT Amount Line
    fields
    {
        field(50000; CodigoGrupoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Código Grupo IRPF', Comment = 'ESP="Código Grupo IRPF"';
            TableRelation = GruposIRPF.CodigoIRPF;
        }
        field(50001; TipoCalculoIRPF; Enum TipoCalculoIRPF)
        {
            DataClassification = CustomerContent;
            Caption = 'Tipo Cálculo IRPF', Comment = 'ESP="Tipo Cálculo IRPF"';
            Editable = false;
        }
        field(50002; ImporteIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Importe IRPF', Comment = 'ESP="Importe IRPF"';
        }
        field(50003; BaseIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Base IRPF', Comment = 'ESP="Base IRPF"';
        }
        field(50004; PorcentajeRetencionIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = '% Retención', Comment = 'ESP="% Retención"';
            MinValue = 0;
            MaxValue = 100;
            Editable = false;
        }
        field(50005; TotalImporteSinIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Total Importe Sin IRPF', Comment = 'ESP="Total Importe Sin IRPF"';
        }
    }
    procedure GetTotalIRPF(): Decimal
    var
        TotalIRPF: Decimal;
    begin
        TotalIRPF := 0;
        IF Rec.Find('-') then
            repeat
                TotalIRPF := TotalIRPF + Rec.ImporteIRPF;
            until Rec.Next() = 0;
        EXIT(TotalIRPF);
    end;
    //#endregion
}