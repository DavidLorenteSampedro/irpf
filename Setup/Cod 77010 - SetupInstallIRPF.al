codeunit 77010 "SetupInstallIRPF"
{
    Subtype = Install;

    trigger OnInstallAppPerCompany()
    var
        info: ModuleInfo;
    begin
        NavApp.GetCurrentModuleInfo(info);
        InstallIRPF();
    end;

    local procedure InstallIRPF()
    var
        PurchasesPayablesSetup: Record "Purchases & Payables Setup";
    begin
        if not PurchasesPayablesSetup.Get() then
            exit;
        PurchasesPayablesSetup.AvisoSetup1IRPF := false;
        PurchasesPayablesSetup.AvisoSetup2IRPF := false;
        if not PurchasesPayablesSetup.Modify() then;
    end;
}