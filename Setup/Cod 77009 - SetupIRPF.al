codeunit 77009 "SetupIRPF"
{
    var
        Notification001Lbl: label 'Compras_1', Comment = 'ESP="Compras_1"';
        Notification002Lbl: label 'Compras_2', Comment = 'ESP="Compras_2"';
        Notification003Lbl: label 'Diarios_1', Comment = 'ESP="Diarios_1"';
        NotificationSetup001Lbl: label 'El módulo IRPF de compras no esta activado.', Comment = 'ESP="El módulo IRPF de compras no esta activado."';
        NotificationSetup002Lbl: label 'No hay ningún grupo contable de producto que soporte IRPF', Comment = 'ESP="No hay ningún grupo contable de producto que soporte IRPF"';
        NotificationSetup003Lbl: label 'No esta configurado el libro y la sección para liquidar el IRPF', Comment = 'ESP="No esta configurado el libro y la sección para liquidar el IRPF"';
        NotificationSetupText001Lbl: label 'Setup', Comment = 'ESP="Configuración"';
        NotificationSetupText002Lbl: label 'Setup', Comment = 'ESP="Configuración"';
        NotificationSetupText003Lbl: label 'Setup', Comment = 'ESP="Configuración"';
        NotificationSetupNoMoreLbl: label 'No mostrar más', Comment = 'ESP="No mostrar más"';

    procedure SetupActive(PurchaseHeader: Record "Purchase Header")
    var
        PurchasesPayablesSetup: Record "Purchases & Payables Setup";
    begin
        if not PurchasesPayablesSetup.Get() then
            exit;
        if PurchasesPayablesSetup.AvisoSetup1IRPF then
            exit;
        if PurchasesPayablesSetup.ActivoIRPF then
            exit;
        SetUpNotification(Notification001Lbl, NotificationSetup001Lbl, NotificationSetupText001Lbl);
    end;

    procedure SetupAccounts(PurchaseHeader: Record "Purchase Header")
    var
        PurchasesPayablesSetup: Record "Purchases & Payables Setup";
        GenProductPostingGroup: Record "Gen. Product Posting Group";
    begin
        if not PurchasesPayablesSetup.Get() then
            exit;
        if PurchasesPayablesSetup.AvisoSetup2IRPF then
            exit;
        GenProductPostingGroup.Reset();
        GenProductPostingGroup.SetRange(SoportaIRPF, true);
        if GenProductPostingGroup.IsEmpty() then
            SetUpNotification(Notification002Lbl, NotificationSetup002Lbl, NotificationSetupText002Lbl);
    end;

    procedure SetupJournalsTemplates()
    var
        PurchasesPayablesSetup: Record "Purchases & Payables Setup";
    begin
        if not PurchasesPayablesSetup.Get() then
            exit;
        if PurchasesPayablesSetup.AvisoSetup3IRPF then
            exit;
        if (PurchasesPayablesSetup.LibroDiarioIRPF = '') or (PurchasesPayablesSetup.SeccionDiarioIRPF = '') then
            SetUpNotification(Notification003Lbl, NotificationSetup003Lbl, NotificationSetupText003Lbl);
    end;

    procedure SetUpNotification(IRPF: Text; MessageText: Text; SetUpText: Text)
    var
        Notificacion: Notification;
    begin
        Notificacion.Id(CreateGuid());
        Notificacion.Message := MessageText;
        Notificacion.SCOPE := NOTIFICATIONSCOPE::LocalScope;
        Notificacion.AddAction(SetUpText, Codeunit::SetupIRPF, 'SetUpNotificationManual');
        Notificacion.AddAction(NotificationSetupNoMoreLbl, Codeunit::SetupIRPF, 'SetUpNotificationManualNoMore');
        Notificacion.SetData('IRPF', IRPF);
        Notificacion.Send();
    end;

    procedure SetUpNotificationManual(Notificacion: Notification)
    var
        PurchasesPayablesSetup: Record "Purchases & Payables Setup";
        GenProductPostingGroup: Record "Gen. Product Posting Group";
    begin
        if Notificacion.GetData('IRPF') = Notification001Lbl then begin
            Page.Run(0, PurchasesPayablesSetup);
        end;
        if Notificacion.GetData('IRPF') = Notification002Lbl then begin
            Page.Run(0, GenProductPostingGroup);
        end;
        if Notificacion.GetData('IRPF') = Notification003Lbl then begin
            Page.Run(0, PurchasesPayablesSetup);
        end;
    end;

    procedure SetUpNotificationManualNoMore(Notificacion: Notification)
    var
        PurchasesPayablesSetup: Record "Purchases & Payables Setup";
    begin
        PurchasesPayablesSetup.Get();
        if Notificacion.GetData('IRPF') = Notification001Lbl then begin
            PurchasesPayablesSetup.AvisoSetup1IRPF := true;
        end;
        if Notificacion.GetData('IRPF') = Notification002Lbl then begin
            PurchasesPayablesSetup.AvisoSetup2IRPF := true;
        end;
        if Notificacion.GetData('IRPF') = Notification003Lbl then begin
            PurchasesPayablesSetup.AvisoSetup3IRPF := true;
        end;
        PurchasesPayablesSetup.Modify();
    end;
}