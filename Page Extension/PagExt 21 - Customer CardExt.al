pageextension 77000 "CustomerCardIRPF" extends "Customer Card" //21
{
    layout
    {
        addafter("Customer Posting Group")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
            }
        }
    }
}