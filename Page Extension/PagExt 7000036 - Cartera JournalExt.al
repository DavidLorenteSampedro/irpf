pageextension 77030 "CarteraJournalIRPF" extends "Cartera Journal" //7000036
{
    layout
    {
        addafter("Posting Date")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
            }
            field(CodigoGrupoContrapartidaIRPF; CodigoGrupoContrapartidaIRPF)
            {
                ApplicationArea = All;
            }
            field(TipoCalculoIRPF; TipoCalculoIRPF)
            {
                ApplicationArea = All;
            }
            field(BaseIRPF; BaseIRPF)
            {
                ApplicationArea = All;
            }
            field(ImporteIRPF; ImporteIRPF)
            {
                ApplicationArea = All;
            }
            field(ImporteDLIRPF; ImporteDLIRPF)
            {
                ApplicationArea = All;
            }
        }
    }
}