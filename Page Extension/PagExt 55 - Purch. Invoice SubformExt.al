pageextension 77013 "PurchInvoiceSubformIRPF" extends "Purch. Invoice Subform" //55
{
    //#region Purchase Invoice Subform
    layout
    {
        addafter("VAT Prod. Posting Group")
        {
            field(CalculaIRPF; CalculaIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(PorcentajeRetencionIRPF; PorcentajeRetencionIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(TipoCalculoIRPF; TipoCalculoIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(BaseIRPF; BaseIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(ImporteIRPF; ImporteIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
        }
        addafter(Control15)
        {
            field(TotalBaseIRPF; decBaseIRPF)
            {
                Caption = 'Base IRPF', Comment = 'ESP="Base IRPF"';
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
                Editable = false;
            }

            field(TotalImporteIRPF; decImporteIRPF)
            {
                Caption = 'Importe IRPF', Comment = 'ESP="Importe IRPF"';
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
                Editable = false;
            }
            field(TotalFacturaSinIRPF; decTotalFacturaSinIRPF)
            {
                Caption = 'Importe total incl. IVA sin IRPF', Comment = 'ESP="Importe total incl. IVA sin IRPF"';
                ApplicationArea = All;
                Visible = boolVisibleTotalIRPF;
                Editable = false;
            }

        }
    }
    trigger OnOpenPage()
    var
        MgtIRPF: Codeunit MgtIRPF;
    begin
        boolVisibleIRPF := MgtIRPF.ActivePurchaseIRPF();
        boolVisibleTotalIRPF := MgtIRPF.ShowTotalPurchasesIRPF();
    end;

    trigger OnAfterGetCurrRecord()
    var
        TotalPurchaseLine: Record "Purchase Line";
        DocumentTotals: Codeunit "Document Totals";
        VATAmount: Decimal;
        InvoiceDiscountAmount: Decimal;
        InvoiceDiscountPct: Decimal;
    begin
        decPorcentajeIRPF := 0;
        decImporteIRPF := 0;
        decBaseIRPF := 0;
        if PurchaseHeader.Get(Rec."Document Type", Rec."Document No.") then begin
            PurchaseHeader.CalcFields(BaseIRPF, ImporteIRPF);
            decImporteIRPF := PurchaseHeader.ImporteIRPF;
            decBaseIRPF := PurchaseHeader.BaseIRPF;
            decPorcentajeIRPF := PurchaseHeader.PorcentajeIRPF;
            txtTipoIRPF := Format(PurchaseHeader.TipoCalculoIRPF);
            DocumentTotals.CalculatePurchaseSubPageTotals(PurchaseHeader, TotalPurchaseLine, VATAmount, InvoiceDiscountAmount, InvoiceDiscountPct);
            decTotalFacturaSinIRPF := TotalPurchaseLine."Amount Including VAT" - decImporteIRPF;
        end;
    end;

    var
        PurchaseHeader: Record "Purchase Header";
        boolVisibleIRPF: Boolean;
        boolVisibleTotalIRPF: Boolean;
        decImporteIRPF: Decimal;
        decBaseIRPF: Decimal;
        decPorcentajeIRPF: Decimal;
        decTotalFacturaSinIRPF: Decimal;
        txtTipoIRPF: Text;
    //#endregion
}