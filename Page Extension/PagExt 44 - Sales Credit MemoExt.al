pageextension 77006 "SalesCrMemoIRPF" extends "Sales Credit Memo" //44
{
    layout
    {
        addafter("VAT Bus. Posting Group")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
            }
        }
    }
}