pageextension 77019 "PostedPurchInvoiceSubformIRPF" extends "Posted Purch. Invoice Subform" //139
{
    layout
    {
        addafter("Unit Cost (LCY)")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(PorcentajeRetencionIRPF; PorcentajeRetencionIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(TipoCalculoIRPF; TipoCalculoIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(BaseIRPF; BaseIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(ImporteIRPF; ImporteIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
        }
    }
    trigger OnOpenPage()
    var
        MgtIRPF: Codeunit MgtIRPF;
    begin
        boolVisibleIRPF := MgtIRPF.ActivePurchaseIRPF();
    end;

    var
        boolVisibleIRPF: Boolean;
}