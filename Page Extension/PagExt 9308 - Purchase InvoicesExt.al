pageextension 77028 "PurchaseInvoicesIRPF" extends "Purchase Invoices" //9308
{
    //#region Purchase Invoices
    trigger OnOpenPage()
    var
        SetupIRPF: Codeunit SetupIRPF;
    begin
        SetupIRPF.SetupActive(Rec);
        SetupIRPF.SetupAccounts(Rec);
    end;
    //#endregion
}