pageextension 77021 "PostedPurchCrMemSubformIRPF" extends "Posted Purch. Cr. Memo Subform" //141
{
    //#region Posted Purch. Cr. Memo Subfor
    layout
    {
        addafter("Unit Cost (LCY)")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(PorcentajeRetencionIRPF; PorcentajeRetencionIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(TipoCalculoIRPF; TipoCalculoIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(BaseIRPF; BaseIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
            field(ImporteIRPF; ImporteIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
        }
    }
    trigger OnOpenPage()
    var
        MgtIRPF: Codeunit MgtIRPF;
    begin
        boolVisibleIRPF := MgtIRPF.ActivePurchaseIRPF();
    end;

    var
        boolVisibleIRPF: Boolean;
    //#endregion
}