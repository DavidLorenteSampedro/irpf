pageextension 77025 "VATSpecificationSubformIRPF" extends "VAT Specification Subform"  //576
{
    //#region VAT Specification Subform
    layout
    {
        addafter("VAT %")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                Visible = boolVisibleIRPF;
                Editable = false;
                ApplicationArea = All;
            }
            field(PorcentajeRetencionIRPF; PorcentajeRetencionIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
                Editable = false;
            }
            field(TipoCalculoIRPF; TipoCalculoIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
                Editable = false;
            }
            field(BaseIRPF; BaseIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
                Editable = false;
            }
            field(ImporteIRPF; ImporteIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
                Editable = false;
            }
            field(TotalImporteSinIRPF; TotalImporteSinIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleTotalIRPF;
                Editable = false;
            }
        }
    }
    trigger OnOpenPage()
    var
        MgtIRPF: Codeunit MgtIRPF;
    begin
        boolVisibleIRPF := MgtIRPF.ActivePurchaseIRPF();
        boolVisibleTotalIRPF := MgtIRPF.ShowTotalPurchasesIRPF();
    end;

    var
        boolVisibleIRPF: Boolean;
        boolVisibleTotalIRPF: Boolean;
    //#endregion
}