pageextension 77026 "FixedAssetGLJournalIRPF" extends "Fixed Asset G/L Journal" //5628
{
    layout
    {
        addafter("Posting Date")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
            }
            field(CodigoGrupoContrapartidaIRPF; CodigoGrupoContrapartidaIRPF)
            {
                ApplicationArea = All;
            }
            field(TipoCalculoIRPF; TipoCalculoIRPF)
            {
                ApplicationArea = All;
            }
            field(BaseIRPF; BaseIRPF)
            {
                ApplicationArea = All;
            }
            field(ImporteIRPF; ImporteIRPF)
            {
                ApplicationArea = All;
            }
            field(ImporteDLIRPF; ImporteDLIRPF)
            {
                ApplicationArea = All;
            }
        }
    }
}