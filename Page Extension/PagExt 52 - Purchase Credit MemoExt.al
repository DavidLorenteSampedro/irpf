pageextension 77012 "PurchaseCrMemoIRPF" extends "Purchase Credit Memo" //52
{
    //#region Purchase Cr. Memo
    layout
    {
        addafter(General)
        {
            group(IRPF)
            {
                Caption = 'IRPF', Comment = 'ESP="IRPF"';
                Visible = boolVisibleIRPF;
                field(CodigoGrupoIRPF; CodigoGrupoIRPF)
                {
                    ApplicationArea = All;
                }
                field(TipoCalculoIRPF; TipoCalculoIRPF)
                {
                    ApplicationArea = All;
                }
                field(PorcentajeIRPF; PorcentajeIRPF)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
    trigger OnOpenPage()
    var
        MgtIRPF: Codeunit MgtIRPF;
        SetupIRPF: Codeunit SetupIRPF;
    begin
        boolVisibleIRPF := MgtIRPF.ActivePurchaseIRPF();
        SetupIRPF.SetupActive(Rec);
        SetupIRPF.SetupAccounts(Rec);
    end;


    var
        boolVisibleIRPF: Boolean;
    //#endregion
}