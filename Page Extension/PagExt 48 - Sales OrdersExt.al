pageextension 77010 "SalesOrdersIRPF" extends "Sales Orders" //48
{
    layout
    {
        addafter(Description)
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
            }
        }
    }
}