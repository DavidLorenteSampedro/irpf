pageextension 77016 "GeneralLedgerSetupIRPF" extends "General Ledger Setup" //118
{
    layout
    {
        addafter(Control1900309501)
        {
            group(IRPF)
            {
                Caption = 'IRPF', Comment = 'ESP="IRPF"';
                field(MaximaDiferenciaIRPF; MaximaDiferenciaIRPF)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}