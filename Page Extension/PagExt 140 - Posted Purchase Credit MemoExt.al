pageextension 77020 "PostedPurchaseCrMemoIRPF" extends "Posted Purchase Credit Memo" //140
{
    //#region Posted Purchase Credit Memo
    layout
    {
        addafter(General)
        {
            group(IRPF)
            {
                Caption = 'IRPF', Comment = 'ESP="IRPF"';
                Visible = boolVisibleIRPF;
                field(CodigoGrupoIRPF; CodigoGrupoIRPF)
                {
                    ApplicationArea = All;
                }
                field(TipoCalculoIRPF; TipoCalculoIRPF)
                {
                    ApplicationArea = All;
                }
                field(PorcentajeIRPF; PorcentajeIRPF)
                {
                    ApplicationArea = All;
                }
                field(BaseIRPF; BaseIRPF)
                {
                    ApplicationArea = All;
                }
                field(ImporteIRPF; ImporteIRPF)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
    trigger OnOpenPage()
    var
        MgtIRPF: Codeunit MgtIRPF;
    begin
        boolVisibleIRPF := MgtIRPF.ActivePurchaseIRPF();
    end;

    var
        boolVisibleIRPF: Boolean;
    //#endregion
}