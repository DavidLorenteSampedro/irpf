pageextension 77004 "SalesOrderIRPF" extends "Sales Order" //42
{
    layout
    {
        addafter("VAT Bus. Posting Group")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
            }
        }
    }
}