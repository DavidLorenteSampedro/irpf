pageextension 77009 "SalesInvoiceSubformIRPF" extends "Sales Invoice Subform" //47
{
    layout
    {
        addafter("VAT Prod. Posting Group")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
            }
            field(PorcentajeRetencionIRPF; PorcentajeRetencionIRPF)
            {
                ApplicationArea = All;
            }
            field(TipoCalculoIRPF; TipoCalculoIRPF)
            {
                ApplicationArea = All;
            }
            field(BaseIRPF; BaseIRPF)
            {
                ApplicationArea = All;
            }
            field(ImporteIRPF; ImporteIRPF)
            {
                ApplicationArea = All;
            }
        }
    }
}