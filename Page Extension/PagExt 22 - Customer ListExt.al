pageextension 77001 "CustomerListIRPF" extends "Customer List" //22
{
    layout
    {
        addafter("Customer Posting Group")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
            }
        }
    }
}