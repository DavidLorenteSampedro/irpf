pageextension 77003 "VendorListIRPF" extends "Vendor List" //27
{
    //#region Vendor List
    layout
    {
        addafter("VAT Bus. Posting Group")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
                Visible = boolVisibleIRPF;
            }
        }
    }
    trigger OnOpenPage()
    var
        MgtIRPF: Codeunit MgtIRPF;
    begin
        boolVisibleIRPF := MgtIRPF.ActivePurchaseIRPF();
    end;

    var
        boolVisibleIRPF: Boolean;
    //#endregion
}