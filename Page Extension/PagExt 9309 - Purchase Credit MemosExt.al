pageextension 77029 "PurchaseCreditMemosIRPF" extends "Purchase Credit Memos" //9309
{
    //#region Purchase Invoices
    trigger OnOpenPage()
    var
        SetupIRPF: Codeunit SetupIRPF;
    begin
        SetupIRPF.SetupActive(Rec);
        SetupIRPF.SetupAccounts(Rec);
    end;
    //#endregion
}