pageextension 77002 "VendorCardIRPF" extends "Vendor Card" //26
{
    //#region Vendor Card
    layout
    {
        addafter("VAT Bus. Posting Group")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                Visible = boolVisibleIRPF;
                ApplicationArea = All;
            }
        }
    }
    trigger OnOpenPage()
    var
        MgtIRPF: Codeunit MgtIRPF;
    begin
        boolVisibleIRPF := MgtIRPF.ActivePurchaseIRPF();
    end;

    var
        boolVisibleIRPF: Boolean;
    //#endregion
}