pageextension 77017 "PostedSalesInvoiceSubformIRPF" extends "Posted Sales Invoice Subform" //133
{
    layout
    {
        addafter("Unit Cost (LCY)")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
            }
            field(PorcentajeRetencionIRPF; PorcentajeRetencionIRPF)
            {
                ApplicationArea = All;
            }
            field(TipoCalculoIRPF; TipoCalculoIRPF)
            {
                ApplicationArea = All;
            }
            field(BaseIRPF; BaseIRPF)
            {
                ApplicationArea = All;
            }
            field(ImporteIRPF; ImporteIRPF)
            {
                ApplicationArea = All;
            }
        }
    }
}