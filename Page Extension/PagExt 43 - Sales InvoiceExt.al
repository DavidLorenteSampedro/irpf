pageextension 77005 "SalesInvoiceIRPF" extends "Sales Invoice" //43
{
    layout
    {
        addafter("VAT Bus. Posting Group")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
            }
        }
    }
}