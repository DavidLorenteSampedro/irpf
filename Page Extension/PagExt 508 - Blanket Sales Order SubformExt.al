pageextension 77024 "BlanketSalesOrderSubformIRPF" extends "Blanket Sales Order Subform"  //508
{
    layout
    {
        addafter("Unit Cost (LCY)")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
            }
            field(PorcentajeRetencionIRPF; PorcentajeRetencionIRPF)
            {
                ApplicationArea = All;
            }
            field(TipoCalculoIRPF; TipoCalculoIRPF)
            {
                ApplicationArea = All;
            }
            field(BaseIRPF; BaseIRPF)
            {
                ApplicationArea = All;
            }
            field(ImporteIRPF; ImporteIRPF)
            {
                ApplicationArea = All;
            }
        }
    }
}