pageextension 77007 "SalesListIRPF" extends "Sales List" //45
{
    layout
    {
        addafter("Ship-to Post Code")
        {
            field(CodigoGrupoIRPF; CodigoGrupoIRPF)
            {
                ApplicationArea = All;
            }
        }
    }
}