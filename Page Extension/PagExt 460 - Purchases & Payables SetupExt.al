pageextension 77023 "PurchasesPayablesSetupIRPF" extends "Purchases & Payables Setup" //460
{
    layout
    {
        addafter(General)
        {
            group(IRPF)
            {
                Caption = 'IRPF', Comment = 'ESP="IRPF"';
                field(ActivoIRPF; ActivoIRPF)
                {
                    ApplicationArea = All;
                }
                field(MostrarTotalFacturaSinIRPF; MostrarTotalFacturaSinIRPF)
                {
                    ApplicationArea = All;
                }
                field(LibroDiarioIRPF; LibroDiarioIRPF)
                {
                    ApplicationArea = All;
                }
                field(SeccionDiarioIRPF; SeccionDiarioIRPF)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}