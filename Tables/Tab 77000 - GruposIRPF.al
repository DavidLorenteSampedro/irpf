table 77000 "GruposIRPF"
{
    //#region Grupos IRPF
    Caption = 'Grupos IRPF', Comment = 'ESP="Grupos IRPF"';
    LookupPageId = GruposIRPF;
    DrillDownPageId = GruposIRPF;
    fields
    {
        field(50000; CodigoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Código', Comment = 'ESP="Código"';

        }
        field(50001; DescripcionIRPF; Text[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Descripción', Comment = 'ESP="Descripción"';

        }
        field(50002; TipoCalculoIRPF; Enum TipoCalculoIRPF)
        {
            DataClassification = CustomerContent;
            Caption = 'Tipo Calculo', Comment = 'ESP="Tipo Calculo"';
        }
        field(50003; PorcentajeRetencionIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = '% Retención', Comment = 'ESP="% Retención"';
            MinValue = 0;
            MaxValue = 100;
        }
        field(50004; CuentaRetencionSoportadoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Cuenta Retención Soportado', Comment = 'ESP="Cuenta Retención Soportado"';
            TableRelation = "G/L Account"."No.";
        }
        field(50005; CuentaRetencionRepercutidoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Cuenta Retención Repercutido', Comment = 'ESP="Cuenta Retención Repercutido"';
            TableRelation = "G/L Account"."No.";
        }
        field(50006; ImporteIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Importe', Comment = 'ESP="Importe"';
            Editable = false;
        }
        field(50007; BaseIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Base', Comment = 'ESP="Base"';
            Editable = false;
        }
        field(50008; ImporteDLIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Importe (DL)', Comment = 'ESP="Importe (DL)"';
            Editable = false;
        }
        field(50009; BaseDLIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Base (DL)', Comment = 'ESP="Base (DL)"';
            Editable = false;
        }
    }

    keys
    {
        key(PK50000; CodigoIRPF)
        {
            Clustered = true;
        }
    }
    procedure LimpiaImportes()
    begin
        ModifyAll(ImporteIRPF, 0);
        ModifyAll(BaseIRPF, 0);
        ModifyAll(ImporteDLIRPF, 0);
        ModifyAll(BaseDLIRPF, 0);
    end;
    //#endregion
}