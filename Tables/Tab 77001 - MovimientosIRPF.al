table 77001 "MovimientosIRPF"
{
    //#region Movimienos IRPF
    Caption = 'Movimientos IRPF', Comment = 'ESP="Movimientos IRPF"';
    LookupPageId = MovimientosIRPF;
    DrillDownPageId = MovimientosIRPF;

    fields
    {
        field(50000; NoMovimientoIRPF; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'No. Movimiento', Comment = 'ESP="No. Movimiento"';

        }
        field(50001; FechaRegistroIRPF; Date)
        {
            DataClassification = CustomerContent;
            Caption = 'Fecha Registro', Comment = 'ESP="Fecha Registro"';

        }
        field(50002; NoDocumentoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'No. Documento', Comment = 'ESP="No. Documento"';

        }
        field(50003; TipoCalculo; Enum TipoCalculoIRPF)
        {
            DataClassification = CustomerContent;
            Caption = 'Tipo Calculo', Comment = 'ESP="Tipo Calculo"';

        }
        field(50004; TipoIRPF; Enum TipoIRPF)
        {
            DataClassification = CustomerContent;
            Caption = 'Tipo', Comment = 'ESP="Tipo"';

        }
        field(50005; NoProveedorClienteIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'No. Proveedor/Cliente', Comment = 'ESP="No. Proveedor/Cliente"';
            TableRelation = IF (TipoIRPF = CONST(Compras)) Vendor."No." ELSE
            IF (TipoIRPF = CONST(Ventas)) Customer."No.";
        }
        field(50006; GrupoIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Grupo IRPF', Comment = 'ESP="Grupo IRPF"';
            TableRelation = GruposIRPF.CodigoIRPF;
        }
        field(50007; PorcentajeRetencionIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Porcentaje Retención IRPF', Comment = 'ESP="Porcentaje Retención IRPF"';
            MinValue = 0;
            MaxValue = 100;
        }
        field(50008; BaseIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Base IRPF', Comment = 'ESP="Base IRPF"';
        }
        field(50009; ImporteIRPF; Decimal)
        {
            DataClassification = CustomerContent;
            Caption = 'Importe IRPF', Comment = 'ESP="Importe IRPF"';
        }
        field(50010; PendienteIRPF; Boolean)
        {
            DataClassification = CustomerContent;
            Caption = 'Pendiente', Comment = 'ESP="Pendiente"';
            InitValue = true;
        }
        field(50011; FechaLiquidacionIRPF; Date)
        {
            DataClassification = CustomerContent;
            Caption = 'Fecha Liquidación', Comment = 'ESP="Fecha Liquidación"';
        }
        field(50012; TipoDocumentoIRPF; Enum TipoDocumentoIRPF)
        {
            DataClassification = CustomerContent;
            Caption = 'Tipo Documento', Comment = 'ESP="Tipo Documento"';
        }
        field(50013; GuidMovimientoIRPF; Guid)
        {
            DataClassification = CustomerContent;
        }
        field(50014; NoDocumentoLiquidacionIRPF; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'No. Documento Liquidación', Comment = 'ESP="No. Documento Liquidación"';

        }
    }

    keys
    {
        key(PK50000; NoMovimientoIRPF)
        {
            Clustered = true;
        }
        key(PK50001; NoDocumentoIRPF, FechaRegistroIRPF)
        {

        }
        key(PK50002; GuidMovimientoIRPF)
        {

        }
        key(PK50003; NoDocumentoLiquidacionIRPF, FechaLiquidacionIRPF)
        {

        }
    }
    //#endregion
}