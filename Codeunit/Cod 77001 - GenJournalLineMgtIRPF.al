codeunit 77001 "GenJournalLineMgtIRPF"
{
    var
        Currency: Record Currency;
        GeneralLedgerSetup: Record "General Ledger Setup";
        SingleInstanceIRPF: Codeunit SingleInstanceIRPF;
        CurrencyCode: Code[10];

    //#region Eventos
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Gen. Jnl.-Post Batch", 'OnBeforeThrowPreviewError', '', true, true)]
    local procedure OnBeforeThrowPreviewError()
    begin
        //Control del PreviewMode si da error lo ponemos en blanco de nuevo
        SingleInstanceIRPF.SetPreviewMode('');
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Gen. Jnl.-Post Batch", 'OnBeforeProcessLines', '', true, true)]
    local procedure OnBeforeProcessLines(var GenJournalLine: Record "Gen. Journal Line"; PreviewMode: Boolean)
    begin
        //Control del PreviewMode si da error lo ponemos en blanco de nuevo
        if PreviewMode then
            SingleInstanceIRPF.SetPreviewMode(GenJournalLine."Document No.")
        else
            SingleInstanceIRPF.SetPreviewMode('');
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Gen. Jnl.-Post Batch", 'OnBeforePostGenJnlLine', '', true, true)]
    local procedure OnBeforePostGenJnlLine(var GenJournalLine: Record "Gen. Journal Line")
    begin
        //Marcamos los movimientos como liquidados
        if SingleInstanceIRPF.GetPreviewMode() <> '' then
            exit;
        if ISNULLGUID(GenJournalLine.GuidMovimientoIRPF) = false then
            MarcarMovimientosIRPF(GenJournalLine);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnBeforeValidateEvent', 'Account Type', true, true)]
    local procedure OnBeforeValidateEventAccountType(var Rec: Record "Gen. Journal Line"; var xRec: Record "Gen. Journal Line"; CurrFieldNo: Integer)
    begin
        //Vaciamos el codigo de IRPF
        if Rec."Account Type" in [Rec."Account Type"::Customer, Rec."Account Type"::Vendor, Rec."Account Type"::"Bank Account"] then
            Rec.Validate(Rec.CodigoGrupoIRPF, '');
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnBeforeValidateEvent', 'Account No.', true, true)]
    local procedure OnBeforeValidateEventAccountNo(var Rec: Record "Gen. Journal Line"; var xRec: Record "Gen. Journal Line"; CurrFieldNo: Integer)
    begin
        //Vaciamos el codigo de IRPF
        if Rec."Account No." <> xRec."Account No." then
            Rec.Validate(Rec.CodigoGrupoIRPF, '');
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnAfterAccountNoOnValidateGetGLAccount', '', true, true)]
    procedure OnAfterAccountNoOnValidateGetGLAccount(var GenJournalLine: Record "Gen. Journal Line"; var GLAccount: Record "G/L Account")
    begin
        //Vaciamos el codigo de IRPF
        GenJournalLine.Validate(GenJournalLine.CodigoGrupoIRPF, '');
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnAfterAccountNoOnValidateGetCustomerAccount', '', true, true)]
    procedure OnAfterAccountNoOnValidateGetCustomerAccount(var GenJournalLine: Record "Gen. Journal Line"; var Customer: Record Customer)
    begin
        //Vaciamos el codigo de IRPF
        GenJournalLine.Validate(GenJournalLine.CodigoGrupoIRPF, '');
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnAfterAccountNoOnValidateGetVendorAccount', '', true, true)]
    procedure OnAfterAccountNoOnValidateGetVendorAccount(var GenJournalLine: Record "Gen. Journal Line"; var Vendor: Record Vendor)
    begin
        //Vaciamos el codigo de IRPF
        GenJournalLine.Validate(GenJournalLine.CodigoGrupoIRPF, '');
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnAfterAccountNoOnValidateGetBankAccount', '', true, true)]
    procedure OnAfterAccountNoOnValidateGetBankAccount(var GenJournalLine: Record "Gen. Journal Line"; var BankAccount: Record "Bank Account")
    begin
        //Vaciamos el codigo de IRPF
        GenJournalLine.Validate(GenJournalLine.CodigoGrupoIRPF, '');
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnBeforeValidateEvent', 'Bal. Account No.', true, true)]
    procedure OnBeforeValidateEventBalAccountNo(var Rec: Record "Gen. Journal Line"; var xRec: Record "Gen. Journal Line"; CurrFieldNo: Integer)
    begin
        //Vaciamos el codigo de IRPF
        Rec.Validate(CodigoGrupoContrapartidaIRPF, '');
        if Rec."Bal. Account No." = '' then
            if xRec."Bal. Account No." <> '' then
                Rec.Validate(CodigoGrupoContrapartidaIRPF, '');
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnAfterValidateEvent', 'Amount', true, true)]
    procedure OnAfterValidateEventAmount(var Rec: Record "Gen. Journal Line"; var xRec: Record "Gen. Journal Line"; CurrFieldNo: Integer)
    begin
        //Generamos los importes del IRPF
        if Rec.CodigoGrupoIRPF <> '' then
            GenerarIRPF(Rec)
        else
            if Rec.CodigoGrupoContrapartidaIRPF <> '' then
                GenerarContrapartidaIRPF(Rec);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnAfterValidateEvent', 'CodigoGrupoIRPF', true, true)]
    procedure OnAfterValidateEventCodigoGrupoIRPF(var Rec: Record "Gen. Journal Line"; var xRec: Record "Gen. Journal Line"; CurrFieldNo: Integer)
    begin
        //Generamos los importes del IRPF
        if (Rec."Document Type" <> Rec."Document Type"::Invoice) and (Rec."Document Type" <> Rec."Document Type"::"Credit Memo") then
            Rec.CodigoGrupoIRPF := '';

        if (Rec.CodigoGrupoIRPF <> '') then begin
            case Rec."Account Type" of
                Rec."Account Type"::"Fixed Asset":
                    begin
                        Rec.CodigoGrupoContrapartidaIRPF := '';
                        GenerarIRPF(Rec);
                    end;
                Rec."Account Type"::"G/L Account":
                    begin
                        Rec.CodigoGrupoContrapartidaIRPF := '';
                        GenerarIRPF(Rec);
                    end;
                else
                    Error('Solo puede indicar Grupo IRPF si Tipo Mov. es Cuenta o Activo');
            end;
        end else begin
            Rec.TipoCalculoIRPF := Rec.TipoCalculoIRPF::Blank;
            Rec.ImporteIRPF := 0;
            Rec.ImporteDLIRPF := 0;
            Rec.BaseIRPF := 0;
            Rec.BaseDLIRPF := 0;
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnAfterValidateEvent', 'CodigoGrupoContrapartidaIRPF', true, true)]
    procedure OnAfterValidateEventCodigoGrupoContrapartidaIRPF(var Rec: Record "Gen. Journal Line"; var xRec: Record "Gen. Journal Line"; CurrFieldNo: Integer)
    begin
        //Generamos los importes de la contrapartida del IRPF
        if (Rec."Document Type" <> Rec."Document Type"::Invoice) and (Rec."Document Type" <> Rec."Document Type"::"Credit Memo") then
            Rec.CodigoGrupoContrapartidaIRPF := '';

        if (Rec.CodigoGrupoContrapartidaIRPF <> '') then begin
            case Rec."Account Type" of
                Rec."Account Type"::"Fixed Asset":
                    begin
                        Rec.CodigoGrupoIRPF := '';
                        GenerarContrapartidaIRPF(Rec);
                    end;
                Rec."Account Type"::"G/L Account":
                    begin
                        Rec.CodigoGrupoIRPF := '';
                        GenerarContrapartidaIRPF(Rec);
                    end;
                else
                    Error('Solo puede indicar Grupo IRPF si Tipo Mov. es Cuenta o Activo');
            end;
        end else begin
            Rec.TipoCalculoIRPF := Rec.TipoCalculoIRPF::Blank;
            Rec.ImporteIRPF := 0;
            Rec.ImporteDLIRPF := 0;
            Rec.BaseIRPF := 0;
            Rec.BaseDLIRPF := 0;
        end;
    end;
    //#endregion
    //#region Funciones
    local procedure GenerarIRPF(var Rec: Record "Gen. Journal Line")
    var
        GruposIRPF: Record GruposIRPF;
    begin
        //Importes del IRPF
        if not GruposIRPF.GET(Rec.CodigoGrupoIRPF) then
            Error('El grupo IRPF %1 no existe', Rec.CodigoGrupoIRPF);
        GetCurrency(Rec);

        Rec.TipoCalculoIRPF := GruposIRPF.TipoCalculoIRPF;
        if GruposIRPF.TipoCalculoIRPF = GruposIRPF.TipoCalculoIRPF::Importe then begin
            Rec.BaseIRPF := Rec."VAT Base Amount";
            Rec.BaseDLIRPF := Rec."VAT Base Amount (LCY)";
            Rec.ImporteIRPF := ROUND(Rec.BaseIRPF * GruposIRPF.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
            Rec.ImporteDLIRPF := ROUND(Rec.BaseDLIRPF * GruposIRPF.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
        end else begin
            Rec.BaseIRPF := Rec."VAT Base Amount" + Rec."VAT Amount";
            Rec.BaseDLIRPF := Rec."VAT Base Amount (LCY)" + Rec."VAT Amount (LCY)";
            Rec.ImporteIRPF := ROUND(Rec.BaseIRPF * GruposIRPF.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
            Rec.ImporteDLIRPF := ROUND(Rec.BaseDLIRPF * GruposIRPF.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
        end;
    end;

    local procedure GenerarContrapartidaIRPF(var Rec: Record "Gen. Journal Line")
    var
        GruposIRPF: Record GruposIRPF;
    begin
        //Importes del IRPF
        if not GruposIRPF.GET(Rec.CodigoGrupoContrapartidaIRPF) then
            Error('El grupo IRPF Contrapartida %1 no existe', Rec.CodigoGrupoContrapartidaIRPF);
        GetCurrency(Rec);

        Rec.TipoCalculoIRPF := GruposIRPF.TipoCalculoIRPF;
        if GruposIRPF.TipoCalculoIRPF = GruposIRPF.TipoCalculoIRPF::Importe then begin
            Rec.BaseIRPF := -Rec."Bal. VAT Base Amount";
            Rec.BaseDLIRPF := -Rec."Bal. VAT Base Amount (LCY)";
            Rec.ImporteIRPF := ROUND(Rec.BaseIRPF * GruposIRPF.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
            Rec.ImporteDLIRPF := ROUND(Rec.BaseDLIRPF * GruposIRPF.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
        end else begin
            Rec.BaseIRPF := -Rec."VAT Base Amount" - Rec."VAT Amount";
            Rec.BaseDLIRPF := -Rec."VAT Base Amount (LCY)" - Rec."VAT Amount (LCY)";
            Rec.ImporteIRPF := ROUND(Rec.BaseIRPF * GruposIRPF.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
            Rec.ImporteDLIRPF := ROUND(Rec.BaseDLIRPF * GruposIRPF.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
        end;
    end;

    local procedure GetCurrency(var Rec: Record "Gen. Journal Line")
    begin
        //Funcion auxiliar para obtener Moneda
        GeneralLedgerSetup.Get();
        IF Rec."Additional-Currency Posting" = Rec."Additional-Currency Posting"::"Additional-Currency Amount Only" then
            CurrencyCode := GeneralLedgerSetup."Additional Reporting Currency"
        else
            CurrencyCode := Rec."Currency Code";

        if CurrencyCode = '' then begin
            Clear(Currency);
            Currency.InitRoundingPrecision();
        end else
            if CurrencyCode <> Currency.Code then begin
                Currency.GET(CurrencyCode);
                Currency.TESTFIELD("Amount Rounding Precision");
            end;
    end;

    procedure InitIRPF(var GenJnlLine: Record "Gen. Journal Line");
    begin
        //Inicializamos todos los datos datos del IRPF
        GenJnlLine.CodigoGrupoIRPF := '';
        GenJnlLine.TipoCalculoIRPF := 0;
        GenJnlLine.ImporteIRPF := 0;
        GenJnlLine.BaseIRPF := 0;
        GenJnlLine.ImporteDLIRPF := 0;
        GenJnlLine.BaseDLIRPF := 0;
    end;

    procedure SetIRPF(var GenJnlLine: Record "Gen. Journal Line"; var TotalIRPF: Record GruposIRPF temporary)
    begin
        GenJnlLine.CodigoGrupoIRPF := TotalIRPF.CodigoIRPF;
        GenJnlLine.TipoCalculoIRPF := TotalIRPF.TipoCalculoIRPF;
        GenJnlLine.ImporteIRPF := TotalIRPF.ImporteIRPF;
        GenJnlLine.BaseIRPF := TotalIRPF.BaseIRPF;
        GenJnlLine.ImporteDLIRPF := TotalIRPF.ImporteDLIRPF;
        GenJnlLine.BaseDLIRPF := TotalIRPF.BaseDLIRPF;
    end;

    local procedure MarcarMovimientosIRPF(var GenJournalLine: Record "Gen. Journal Line")
    var
        MovimientosIRPF: Record MovimientosIRPF;
    begin
        MovimientosIRPF.SetRange(GuidMovimientoIRPF, GenJournalLine.GuidMovimientoIRPF);
        MovimientosIRPF.SetRange(PendienteIRPF, true);
        if MovimientosIRPF.FindSet() then begin
            MovimientosIRPF.ModifyAll(NoDocumentoLiquidacionIRPF, GenJournalLine."Document No.");
            MovimientosIRPF.ModifyAll(FechaLiquidacionIRPF, GenJournalLine."Posting Date");
            MovimientosIRPF.ModifyAll(PendienteIRPF, false);
        end;
    end;
    //#endregion
}