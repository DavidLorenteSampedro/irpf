codeunit 77000 "NavigateMgtIRPF"
{
    //#region Navegar
    var
        intCantidad: Integer;
    //#region Eventos
    [EventSubscriber(ObjectType::Page, Page::Navigate, 'OnAfterNavigateFindRecords', '', true, true)]
    local procedure NavigateOnAfterNavigateFindRecords(var DocumentEntry: Record "Document Entry"; DocNoFilter: Text; PostingDateFilter: Text)
    var
        MovimientosIRPF: Record MovimientosIRPF;
    begin
        //Despues de buscar los registros por las tablas
        if not MovimientosIRPF.ReadPermission() then
            exit;
        intCantidad := 0;
        intCantidad := FilterMovimientosIRPF(MovimientosIRPF, DocNoFilter, PostingDateFilter);
        if intCantidad = 0 then
            intCantidad := FilterMovimientos2IRPF(MovimientosIRPF, DocNoFilter, PostingDateFilter);
        InsertIntoDocEntry(DocumentEntry, DATABASE::MovimientosIRPF, 0, MovimientosIRPF.TableCaption(), intCantidad);
    end;

    [EventSubscriber(ObjectType::Page, Page::Navigate, 'OnBeforeNavigateShowRecords', '', true, true)]
    local procedure NavigateOnBeforeNavigateShowRecords(TableID: Integer; var TempDocumentEntry: Record "Document Entry"; var IsHandled: Boolean; DocNoFilter: Text; PostingDateFilter: Text)
    var
        MovimientosIRPF: Record MovimientosIRPF;
    begin
        //Antes de mostrar el registro seleccionado. Solo muestro si es IRPF
        if TableID <> Database::MovimientosIRPF then
            exit;
        if not MovimientosIRPF.ReadPermission() then
            exit;
        IsHandled := true;
        intCantidad := 0;
        intCantidad := FilterMovimientosIRPF(MovimientosIRPF, DocNoFilter, PostingDateFilter);
        if intCantidad = 0 then
            intCantidad := FilterMovimientos2IRPF(MovimientosIRPF, DocNoFilter, PostingDateFilter);
        Page.RUN(0, MovimientosIRPF);
    end;
    //#endregion
    //#region Funciones
    local procedure FilterMovimientosIRPF(var MovimientosIRPF: Record MovimientosIRPF; DocNoFilter: Text; PostingDateFilter: Text): Integer
    begin
        //Aplicamos los filtros de los movimientos de IRPF
        MovimientosIRPF.Reset();
        MovimientosIRPF.SetCurrentKey(NoDocumentoIRPF, FechaRegistroIRPF);
        MovimientosIRPF.SetFilter(NoDocumentoIRPF, DocNoFilter);
        MovimientosIRPF.SetFilter(FechaRegistroIRPF, PostingDateFilter);
        if MovimientosIRPF.IsEmpty() then
            exit(0);
        exit(MovimientosIRPF.Count());
    end;

    local procedure FilterMovimientos2IRPF(var MovimientosIRPF: Record MovimientosIRPF; DocNoFilter: Text; PostingDateFilter: Text): Integer
    begin
        //Aplicamos los filtros de los movimientos de IRPF
        MovimientosIRPF.Reset();
        MovimientosIRPF.SetCurrentKey(NoDocumentoLiquidacionIRPF, FechaLiquidacionIRPF);
        MovimientosIRPF.SetFilter(NoDocumentoLiquidacionIRPF, DocNoFilter);
        MovimientosIRPF.SetFilter(FechaLiquidacionIRPF, PostingDateFilter);
        if MovimientosIRPF.IsEmpty() then
            exit(0);
        exit(MovimientosIRPF.Count());
    end;

    local procedure InsertIntoDocEntry(var TempDocumentEntry: Record "Document Entry" temporary; DocTableID: Integer; DocType: Option; DocTableName: Text; DocNoOfRecords: Integer)
    begin
        //Guardamos los registros encontrados para mostralos en la pantalla de navegar
        if DocNoOfRecords = 0 then
            exit;
        TempDocumentEntry.Init();
        TempDocumentEntry."Entry No." := TempDocumentEntry."Entry No." + 1;
        TempDocumentEntry."Table ID" := DocTableID;
        TempDocumentEntry."Document Type" := DocType;
        TempDocumentEntry."Table Name" := COPYSTR(DocTableName, 1, MAXSTRLEN(TempDocumentEntry."Table Name"));
        TempDocumentEntry."No. of Records" := DocNoOfRecords;
        TempDocumentEntry.Insert();
    end;
    //#endregion
    //#endregion
}