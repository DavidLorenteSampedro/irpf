codeunit 77005 "PurchaseLineMgtIRPF"
{
    //#region Purchase Line Mgt
    var
        PurchaseHeader: Record "Purchase Header";
        Currency: Record Currency;
    //#region Eventos
    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', 'CalculaIRPF', true, true)]
    local procedure OnAfterValidateEventCalculaIRPF(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    var
        GenProdPostingGrp: Record "Gen. Product Posting Group";
    begin
        //Al cambiar el check de CalcularIRPF
        IF (Rec.Type IN [Rec.Type::"G/L Account", Rec.Type::"Fixed Asset"]) THEN begin
            GetPurchaseHeader(Rec);
            IF PurchaseHeader.CodigoGrupoIRPF <> '' THEN begin
                IF GenProdPostingGrp.GET(Rec."Gen. Prod. Posting Group") THEN begin
                    IF GenProdPostingGrp.SoportaIRPF THEN begin
                        if rec.CalculaIRPF = true then
                            Rec.VALIDATE(CodigoGrupoIRPF, PurchaseHeader.CodigoGrupoIRPF)
                        else
                            Rec.VALIDATE(CodigoGrupoIRPF, '');
                    end else begin
                        rec.CalculaIRPF := false;
                        Rec.VALIDATE(CodigoGrupoIRPF, '');
                    end;
                end else begin
                    rec.CalculaIRPF := false;
                    Rec.VALIDATE(CodigoGrupoIRPF, '');
                end;
            end else begin
                rec.CalculaIRPF := false;
                Rec.VALIDATE(CodigoGrupoIRPF, '');
            end;
        end else begin
            rec.CalculaIRPF := false;
            Rec.VALIDATE(CodigoGrupoIRPF, '');
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', 'No.', true, true)]
    local procedure OnAfterValidateEventNo(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    begin
        //Despues de Validar el No. de la linea
        TraeIRPF(Rec, xRec);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', 'Type', true, true)]
    local procedure OnAfterValidateEventDocumentType(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    begin
        //Despues de Validar el Tyipo de linea
        Rec.VALIDATE(CalculaIRPF, true);
        IF not (Rec.Type IN [Rec.Type::"G/L Account", Rec.Type::"Fixed Asset"]) THEN
            Rec.VALIDATE(CalculaIRPF, false);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', 'Qty. to Invoice', true, true)]
    local procedure OnAfterValidateEventQtytoInvoice(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    begin
        //Despues de validar la cantidad a enviar
        IF Rec.CodigoGrupoIRPF <> '' THEN
            Rec.VALIDATE(BaseIRPF);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', 'CodigoGrupoIRPF', true, true)]
    local procedure OnAfterValidateEventCodigoGrupoIRPF(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    var
        Vendor: Record Vendor;
        GruposIRPF: Record GruposIRPF;
    begin
        //Despues de validar el Codigo Grupo IRPF
        if (Rec.CodigoGrupoIRPF <> '') and (Rec.Type IN [Rec.Type::"G/L Account", Rec.Type::"Fixed Asset"]) then begin
            GetPurchaseHeader(Rec);
            Vendor.GET(PurchaseHeader."Buy-from Vendor No.");
            if Vendor.CodigoGrupoIRPF = '' then
                Error('No se puede calcular IRPF si el proveedor no tiene configurado IRPF en su ficha.');

            if GruposIRPF.GET(Rec.CodigoGrupoIRPF) then begin
                Rec.TipoCalculoIRPF := GruposIRPF.TipoCalculoIRPF;
                Rec.PorcentajeRetencionIRPF := GruposIRPF.PorcentajeRetencionIRPF;
                Rec.Validate(BaseIRPF);
            end;
        end else begin
            Rec.CodigoGrupoIRPF := '';
            Rec.TipoCalculoIRPF := Rec.TipoCalculoIRPF::Blank;
            Rec.ImporteIRPF := 0;
            Rec.PorcentajeRetencionIRPF := 0;
            Rec.BaseIRPF := 0;
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', 'ImporteIRPF', true, true)]
    local procedure OnAfterValidateEventImporteIRPF(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    begin
        //Despues de validar el Importe IRPF
        GetPurchaseHeader(Rec);
        Rec.ImporteIRPF := ROUND(Rec.BaseIRPF * Rec.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', 'BaseIRPF', true, true)]
    local procedure OnAfterValidateEventBaseIRPF(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    begin
        //Despues de validar la Base IRPF
        GetPurchaseHeader(Rec);
        if Rec.TipoCalculoIRPF = Rec.TipoCalculoIRPF::Importe then begin
            if PurchaseHeader."Prices Including VAT" then
                Rec.BaseIRPF := ROUND((Rec."Line Amount" - Rec."Inv. Discount Amount") / (1 + ((Rec."VAT %" + Rec."EC %") / 100)), Currency."Amount Rounding Precision")
            else
                Rec.BaseIRPF := Rec."Line Amount" - Rec."Inv. Discount Amount";
            Rec.VALIDATE(ImporteIRPF);
        end else begin
            if Rec.TipoCalculoIRPF = Rec.TipoCalculoIRPF::ImporteIVAIncluido then
                if NOT PurchaseHeader."Prices Including VAT" then
                    Rec.BaseIRPF := ROUND((Rec."Line Amount" - Rec."Inv. Discount Amount") * (1 + ((Rec."VAT %" + Rec."EC %") / 100)), Currency."Amount Rounding Precision")
                else
                    Rec.BaseIRPF := Rec."Line Amount" - Rec."Inv. Discount Amount";
            Rec.VALIDATE(ImporteIRPF);
        end;
    end;
    //#endregion
    //#region Funciones
    local procedure TraeIRPF(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line")
    var
        GenProdPostingGrp: Record "Gen. Product Posting Group";
    begin
        //Calculamos el IRPF
        IF (Rec.Type IN [Rec.Type::"G/L Account", Rec.Type::"Fixed Asset"]) THEN begin
            GetPurchaseHeader(Rec);
            IF PurchaseHeader.CodigoGrupoIRPF <> '' THEN begin
                IF GenProdPostingGrp.GET(Rec."Gen. Prod. Posting Group") THEN begin
                    IF GenProdPostingGrp.SoportaIRPF THEN begin
                        rec.CalculaIRPF := true;
                        Rec.VALIDATE(CodigoGrupoIRPF, PurchaseHeader.CodigoGrupoIRPF)
                    end else begin
                        rec.CalculaIRPF := false;
                        Rec.VALIDATE(CodigoGrupoIRPF, '');
                    end;
                end else begin
                    rec.CalculaIRPF := false;
                    Rec.VALIDATE(CodigoGrupoIRPF, '');
                end;
            end else begin
                rec.CalculaIRPF := false;
                Rec.VALIDATE(CodigoGrupoIRPF, '');
            end;
        end else begin
            rec.CalculaIRPF := false;
            Rec.VALIDATE(CodigoGrupoIRPF, '');
        end;
    end;
    //#region Herramientas comunes
    local procedure GetPurchaseHeader(var Rec: Record "Purchase Line")
    begin
        Rec.TestField("Document No.");
        PurchaseHeader.GET(Rec."Document Type", Rec."Document No.");
        if PurchaseHeader."Currency Code" = '' then
            Currency.InitRoundingPrecision()
        else begin
            PurchaseHeader.TESTFIELD("Currency Factor");
            Currency.GET(PurchaseHeader."Currency Code");
            Currency.TESTFIELD("Amount Rounding Precision");
        end;
    end;
    //#endregion
    //#endregion
    //#endregion
}