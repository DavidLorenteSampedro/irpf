codeunit 77006 "MgtIRPF"
{

    //#region Permisos necesarios
    Permissions = TableData "Vendor LEdger Entry" = rim,
                  TableData "Detailed Vendor Ledg. Entry" = rim;
    //#endregion
    var
        AddCurrency: Record Currency;
        CurrExchRate: Record "Currency Exchange Rate";
        MgtIRPF: Codeunit MgtIRPF;
        SingleInstanceIRPF: Codeunit SingleInstanceIRPF;
        AddCurrencyCode: Code[10];
        CurrencyFactor: Decimal;
        NewCurrencyDate: Date;
        CurrencyDate: Date;
        UseCurrFactorOnly: Boolean;
    //#region Eventos       
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Gen. Jnl.-Post Preview", 'OnAfterUnbindSubscription', '', true, true)]
    local procedure OnAfterUnbindSubscription()
    begin
        //Control del PreviewMode si da error lo ponemos en blanco de nuevo
        SingleInstanceIRPF.SetPreviewMode('');
    end;

    [EventSubscriber(ObjectType::Page, Page::"Purchase Invoice", 'OnBeforeActionEvent', 'Preview', true, true)]
    local procedure OnRunPreviewPurchaseInvoice(var Rec: Record "Purchase Header")
    begin
        //Control del PreviewMode
        SingleInstanceIRPF.SetPreviewMode(Rec."No.");
    end;

    [EventSubscriber(ObjectType::Page, Page::"Purchase Credit Memo", 'OnBeforeActionEvent', 'Preview', true, true)]
    local procedure OnRunPreviewPurchaseCreditMemo(var Rec: Record "Purchase Header")
    begin
        //Control del PreviewMode
        SingleInstanceIRPF.SetPreviewMode(Rec."No.");
    end;

    [EventSubscriber(ObjectType::Page, Page::"Purchase Invoices", 'OnBeforeActionEvent', 'Preview', true, true)]
    local procedure OnRunPreviewPurchaseInvoices(var Rec: Record "Purchase Header")
    begin
        //Control del PreviewMode
        SingleInstanceIRPF.SetPreviewMode(Rec."No.");
    end;

    [EventSubscriber(ObjectType::Page, Page::"Purchase Credit Memos", 'OnBeforeActionEvent', 'Preview', true, true)]
    local procedure OnRunPreviewPurchaseCreditMemos(var Rec: Record "Purchase Header")
    begin
        //Control del PreviewMode
        SingleInstanceIRPF.SetPreviewMode(Rec."No.");
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnBeforePostVendorEntry', '', true, true)]
    local procedure OnBeforePostVendorEntry(var GenJnlLine: Record "Gen. Journal Line"; var TotalPurchLine: Record "Purchase Line"; var PurchHeader: Record "Purchase Header")
    begin
        //Aun no ha registrado el asiento del Proveedor y guardamos el importe IRPF
        if not MgtIRPF.ActivePurchaseIRPF() then
            exit;
        SetValuesIRPF(GenJnlLine, TotalPurchLine, PurchHeader);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnAfterIncrAmount', '', true, true)]
    local procedure OnAfterIncrAmount(PurchLine: Record "Purchase Line"; var TotalPurchLine: Record "Purchase Line")
    begin
        //Calculando en las líneas a registrar el total BaseIRPF y ImporteIRPF
        if not MgtIRPF.ActivePurchaseIRPF() then
            exit;
        TotalPurchLine.BaseIRPF := TotalPurchLine.BaseIRPF + PurchLine.BaseIRPF;
        TotalPurchLine.ImporteIRPF := TotalPurchLine.ImporteIRPF + PurchLine.ImporteIRPF;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Gen. Jnl.-Post Line", 'OnAfterPostVend', '', true, true)]
    local procedure OnAfterPostVend(var TempGLEntryBuf: Record "G/L Entry"; var GenJournalLine: Record "Gen. Journal Line"; var NextEntryNo: Integer; var NextTransactionNo: Integer)
    begin
        //Despues de registrar el asiento del Proveedor metemos las lineas de IRPF
        if not MgtIRPF.ActivePurchaseIRPF() then
            exit;
        PostVendorIRPF(TempGLEntryBuf, GenJournalLine, NextEntryNo, NextTransactionNo);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnAfterUpdatePurchaseHeader', '', true, true)]
    local procedure OnAfterUpdatePurchaseHeader(var PurchInvHeader: Record "Purch. Inv. Header"; var PurchCrMemoHdr: Record "Purch. Cr. Memo Hdr."; GenJnlLineDocType: Integer)
    begin
        //Liquidamos nuestros movimientos cuando el registro de lineas ya ha finalizado y aun no ha registrado el asiento de contrapartida ni los efectos
        if not MgtIRPF.ActivePurchaseIRPF() then
            exit;
        ApplyVendorLedgerEntry(PurchInvHeader, PurchCrMemoHdr, GenJnlLineDocType);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnBeforeCreateCarteraBills', '', true, true)]
    local procedure OnBeforeCreateCarteraBills(PurchHeader: Record "Purchase Header"; var TotalPurchLine: Record "Purchase Line"; VendLedgEntry: Record "Vendor Ledger Entry")
    begin
        //Antes de crear los Efectos de las facturas
        if not MgtIRPF.ActivePurchaseIRPF() then
            exit;
        VendorBillsIRPF(PurchHeader, TotalPurchLine, VendLedgEntry);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnBeforePostBalancingEntry', '', true, true)]
    local procedure OnBeforePostBalancingEntry(var GenJnlLine: Record "Gen. Journal Line"; var TotalPurchLineLCY: Record "Purchase Line")
    begin
        //Antes de generar el asiento de contrapartida
        if not MgtIRPF.ActivePurchaseIRPF() then
            exit;
        VendorBankContrapartidaIRPF(GenJnlLine, TotalPurchLineLCY);
    end;
    //#endregion
    //#region Funciones
    //#region Calcular valor del IRPF
    local procedure SetValuesIRPF(var GenJnlLine: Record "Gen. Journal Line"; var TotalPurchLine: Record "Purchase Line"; var PurchHeader: Record "Purchase Header")
    var
        GenJournalLineMgtIRPF: Codeunit GenJournalLineMgtIRPF;
    begin
        //Calculamos el importe del IRPF
        GenJournalLineMgtIRPF.InitIRPF(GenJnlLine);
        if TotalPurchLine.BaseIRPF = 0 then
            exit;
        CalcValuesIRPF(GenJnlLine, PurchHeader, TotalPurchLine);
    end;

    local procedure CalcValuesIRPF(var GenJnlLine: Record "Gen. Journal Line"; var PurchHeader: Record "Purchase Header"; var TotalPurchLine: Record "Purchase Line")
    var
        TotalIRPF: Record GruposIRPF temporary;
        GenJournalLineMgtIRPF: Codeunit GenJournalLineMgtIRPF;
    begin
        //Obtenemos los totales del IRPF y los ponemos en el diario listo para registrar
        Clear(TotalIRPF);
        TotalIRPF.Reset();
        TotalIRPF.DeleteAll();
        GetTotalIRPF(TotalIRPF, PurchHeader, TotalPurchLine);
        GenJournalLineMgtIRPF.SetIRPF(GenJnlLine, TotalIRPF);
    end;

    local procedure GetTotalIRPF(var TotalIRPF: Record GruposIRPF temporary; var PurchHeader: Record "Purchase Header"; var TotalPurchLine: Record "Purchase Line")
    var
        GruposIRPF: Record GruposIRPF;
        intSigno: Integer;
    begin
        //Obtenemos el total de IRPF de la factura con divisas
        intSigno := 1;
        case PurchHeader."Document Type" of
            PurchHeader."Document Type"::Invoice:
                intSigno := -1;
            PurchHeader."Document Type"::"Credit Memo":
                intSigno := 1;
            else
                intSigno := 1;
        end;
        if not GruposIRPF.Get(PurchHeader.CodigoGrupoIRPF) then
            GruposIRPF.Init();
        TotalIRPF.CodigoIRPF := PurchHeader.CodigoGrupoIRPF;
        TotalIRPF.TipoCalculoIRPF := GruposIRPF.TipoCalculoIRPF;
        TotalIRPF.ImporteIRPF := TotalPurchLine.ImporteIRPF * intSigno;
        TotalIRPF.BaseIRPF := TotalPurchLine.BaseIRPF * intSigno;
        if PurchHeader."Currency Code" = '' then begin
            TotalIRPF.ImporteDLIRPF := TotalIRPF.ImporteIRPF;
            TotalIRPF.BaseDLIRPF := TotalIRPF.BaseIRPF;
        end else begin
            TotalIRPF.ImporteDLIRPF := ROUND(CurrExchRate.ExchangeAmtFCYToLCY(PurchHeader."Posting Date", PurchHeader."Currency Code", TotalIRPF.ImporteIRPF, PurchHeader."Currency Factor"));
            TotalIRPF.BaseDLIRPF := ROUND(CurrExchRate.ExchangeAmtFCYToLCY(PurchHeader."Posting Date", PurchHeader."Currency Code", TotalIRPF.BaseIRPF, PurchHeader."Currency Factor"));
        end;
    end;
    //#endregion
    //#region Crear Asientos
    local procedure PostVendorIRPF(var TempGLEntryBuf: Record "G/L Entry"; var GenJournalLine: Record "Gen. Journal Line"; var NextEntryNo: Integer; var NextTransactionNo: Integer)
    begin
        //Registramos el movimiento de IRPF en G/L Entry -PostIRPF-
        if GenJournalLine.CodigoGrupoIRPF <> '' then
            PostIRPF(TempGLEntryBuf, GenJournalLine, NextEntryNo, NextTransactionNo)
        else
            if GenJournalLine.CodigoGrupoContrapartidaIRPF <> '' then begin
                GenJournalLine.CodigoGrupoIRPF := GenJournalLine.CodigoGrupoContrapartidaIRPF;
                GenJournalLine.ImporteIRPF := -GenJournalLine.ImporteIRPF;
                GenJournalLine.ImporteDLIRPF := -GenJournalLine.ImporteDLIRPF;
                GenJournalLine.BaseIRPF := -GenJournalLine.BaseIRPF;
                GenJournalLine.BaseDLIRPF := -GenJournalLine.BaseDLIRPF;
                PostIRPF(TempGLEntryBuf, GenJournalLine, NextEntryNo, NextTransactionNo);
            end;
    end;

    local procedure PostIRPF(var TempGLEntryBuf: Record "G/L Entry"; var GenJournalLine: Record "Gen. Journal Line"; var NextEntryNo: Integer; var NextTransactionNo: Integer)
    var
        MovimientoIRPF: Record MovimientosIRPF;
    begin
        //Registramos el movimiento en nuestra tabka personalizada y luego en G/L Entry
        if (GenJournalLine.CodigoGrupoIRPF = '') or (GenJournalLine.ImporteIRPF = 0) then
            exit;
        //Movimiento personalizado______________________
        PostMovimientoIRPF(MovimientoIRPF, GenJournalLine, NextEntryNo);
        //Movimiento de G/L Entry_______________________
        case MovimientoIRPF.TipoIRPF of
            MovimientoIRPF.TipoIRPF::Compras:
                PostGlEntryPurchaseIRPF(TempGLEntryBuf, MovimientoIRPF, GenJournalLine, NextEntryNo, NextTransactionNo);
        end;
    end;

    local procedure PostMovimientoIRPF(var MovimientoIRPF: Record MovimientosIRPF; GenJournalLine: Record "Gen. Journal Line"; var NextEntryNo: Integer)
    var
        GruposIRPF: Record GruposIRPF;
    begin
        //Movimiento de nuestra tabla personalizada
        if not GruposIRPF.Get(GenJournalLine.CodigoGrupoIRPF) then
            exit;
        MovimientoIRPF.Init();
        MovimientoIRPF.NoMovimientoIRPF := NextEntryNo;
        MovimientoIRPF.FechaRegistroIRPF := GenJournalLine."Posting Date";
        case GenJournalLine."Account Type" of
            GenJournalLine."Account Type"::Customer:
                begin
                    MovimientoIRPF.TipoIRPF := MovimientoIRPF.TipoIRPF::Ventas;
                end;
            GenJournalLine."Account Type"::Vendor:
                begin
                    MovimientoIRPF.TipoIRPF := MovimientoIRPF.TipoIRPF::Compras;
                    if GenJournalLine.ImporteDLIRPF <= 0 then
                        MovimientoIRPF.TipoDocumentoIRPF := MovimientoIRPF.TipoDocumentoIRPF::Factura
                    else
                        MovimientoIRPF.TipoDocumentoIRPF := MovimientoIRPF.TipoDocumentoIRPF::Abono;
                end;
        end;
        MovimientoIRPF.NoDocumentoIRPF := GenJournalLine."Document No.";
        MovimientoIRPF.GrupoIRPF := GenJournalLine.CodigoGrupoIRPF;
        MovimientoIRPF.TipoCalculo := GenJournalLine.TipoCalculoIRPF;
        MovimientoIRPF.NoProveedorClienteIRPF := GenJournalLine."Account No.";
        MovimientoIRPF.BaseIRPF := GenJournalLine.BaseDLIRPF;
        MovimientoIRPF.ImporteIRPF := GenJournalLine.ImporteDLIRPF;
        MovimientoIRPF.PorcentajeRetencionIRPF := GruposIRPF.PorcentajeRetencionIRPF;
        MovimientoIRPF.PendienteIRPF := true;
        MovimientoIRPF.Insert();
    end;

    local procedure PostGlEntryPurchaseIRPF(var TempGLEntryBuf: Record "G/L Entry"; var MovimientoIRPF: Record MovimientosIRPF; GenJournalLine: Record "Gen. Journal Line"; var NextEntryNo: Integer; var NextTransactionNo: Integer)
    var
        GruposIRPF: Record GruposIRPF;
        VendLedgEntry: Record "Vendor Ledger Entry";
        TempDtldCVLedgEntryBuf: Record "Detailed CV Ledg. Entry Buffer" temporary;
        CVLedgEntryBuf: Record "CV Ledger Entry Buffer" temporary;
        DtldVendLedgEntry: Record "Detailed Vendor Ledg. Entry";
        GLEntry: Record "G/L Entry";
        VendorPostingGroup: Record "Vendor Posting Group";
        PurchInvHeader: Record "Purch. Inv. Header";
        PurchCrMemoHd: Record "Purch. Cr. Memo Hdr.";
        Offset: Integer;
    begin
        if not GruposIRPF.Get(GenJournalLine.CodigoGrupoIRPF) then
            exit;
        //#region Compras__________________________________________
        case MovimientoIRPF.TipoDocumentoIRPF of
            MovimientoIRPF.TipoDocumentoIRPF::Factura:
                begin
                    if not PurchInvHeader.Get(GenJournalLine."Document No.") then
                        exit;
                    if not VendorPostingGroup.Get(PurchInvHeader."Vendor Posting Group") then
                        VendorPostingGroup.Init();
                end;
            MovimientoIRPF.TipoDocumentoIRPF::Abono:
                begin
                    if not PurchCrMemoHd.Get(GenJournalLine."Document No.") then
                        exit;
                    if not VendorPostingGroup.Get(PurchCrMemoHd."Vendor Posting Group") then
                        VendorPostingGroup.Init();
                end;
        end;
        GruposIRPF.TestField(CuentaRetencionSoportadoIRPF);
        //#region Movimiento al Debe del IRPF en G/L Entry
        GetCurrencyExchRate(GenJournalLine);
        TempGLEntryBuf.Init();
        TempGLEntryBuf.CopyFromGenJnlLine(GenJournalLine);
        TempGLEntryBuf."Entry No." := NextEntryNo;
        TempGLEntryBuf."Transaction No." := NextTransactionNo;
        TempGLEntryBuf."G/L Account No." := GruposIRPF.CuentaRetencionSoportadoIRPF;
        TempGLEntryBuf."System-Created Entry" := true;
        TempGLEntryBuf.Amount := GenJournalLine.ImporteIRPF;
        TempGLEntryBuf."Additional-Currency Amount" := GLCalcAddCurrency(GLEntry.Amount, 0, GLEntry."Additional-Currency Amount", false, GenJournalLine);
        TempGLEntryBuf.UpdateDebitCredit(GenJournalLine.Correction);
        TempGLEntryBuf.Insert();
        NextEntryNo := NextEntryNo + 1;
        //#endregion
        //#region Movimiento al Haber del IRPF en G/L Entry                  
        TempGLEntryBuf.Init();
        TempGLEntryBuf.CopyFromGenJnlLine(GenJournalLine);
        TempGLEntryBuf."Document Type" := TempGLEntryBuf."Document Type"::" ";
        TempGLEntryBuf."Entry No." := NextEntryNo;
        TempGLEntryBuf."Transaction No." := NextTransactionNo;
        TempGLEntryBuf."G/L Account No." := VendorPostingGroup."Payables Account";
        TempGLEntryBuf."System-Created Entry" := true;
        TempGLEntryBuf.Amount := -GenJournalLine.ImporteIRPF;
        TempGLEntryBuf."External Document No." := GenJournalLine."External Document No.";
        TempGLEntryBuf."No. Series" := GenJournalLine."Posting No. Series";
        TempGLEntryBuf."Additional-Currency Amount" := GLCalcAddCurrency(GLEntry.Amount, 0, GLEntry."Additional-Currency Amount", false, GenJournalLine);
        TempGLEntryBuf.UpdateDebitCredit(GenJournalLine.Correction);
        TempGLEntryBuf."Dimension Set ID" := GenJournalLine."Dimension Set ID";
        TempGLEntryBuf."Global Dimension 1 Code" := GenJournalLine."Shortcut Dimension 1 Code";
        TempGLEntryBuf."Global Dimension 2 Code" := GenJournalLine."Shortcut Dimension 2 Code";
        TempGLEntryBuf.Insert();
        NextEntryNo := NextEntryNo + 1;
        //#endregion
        //#region Movmiento del IRPF del proveedor en Mov. Proveedor
        VendLedgEntry.Init();
        VendLedgEntry.CopyFromGenJnlLine(GenJournalLine);
        VendLedgEntry."Document Type" := VendLedgEntry."Document Type"::" ";
        VendLedgEntry."Entry No." := TempGLEntryBuf."Entry No.";
        VendLedgEntry."Transaction No." := NextTransactionNo;
        VendLedgEntry."Dimension Set ID" := GenJournalLine."Dimension Set ID";
        VendLedgEntry."Global Dimension 1 Code" := GenJournalLine."Shortcut Dimension 1 Code";
        VendLedgEntry."Global Dimension 2 Code" := GenJournalLine."Shortcut Dimension 2 Code";
        VendLedgEntry.Open := true;
        if MovimientoIRPF.TipoDocumentoIRPF = MovimientoIRPF.TipoDocumentoIRPF::Factura then
            VendLedgEntry.Positive := true
        else
            VendLedgEntry.Positive := false;
        VendLedgEntry."Pmt. Discount Date" := 0D;
        if MovimientoIRPF.TipoDocumentoIRPF = MovimientoIRPF.TipoDocumentoIRPF::Factura then
            VendLedgEntry."Applies-to Doc. Type" := VendLedgEntry."Applies-to Doc. Type"::Invoice
        else
            VendLedgEntry."Applies-to Doc. Type" := VendLedgEntry."Applies-to Doc. Type"::"Credit Memo";
        VendLedgEntry."Applying Entry" := false;
        VendLedgEntry."Purchase (LCY)" := 0;
        VendLedgEntry."Amount (LCY) stats." := -GenJournalLine.ImporteDLIRPF;
        VendLedgEntry."Remaining Amount (LCY) stats." := -GenJournalLine.ImporteDLIRPF;
        IF GenJournalLine."Currency Code" <> '' THEN BEGIN
            VendLedgEntry."Original Currency Factor" := GenJournalLine."Currency Factor";
        END ELSE
            VendLedgEntry."Original Currency Factor" := 1;
        VendLedgEntry."Adjusted Currency Factor" := VendLedgEntry."Original Currency Factor";
        VendLedgEntry.Insert();
        //#endregion
        //#region Movimientos detallados del IRPF en Mov. Proveedor Detallados
        TempDtldCVLedgEntryBuf.DELETEALL();
        TempDtldCVLedgEntryBuf.INIT();
        TempDtldCVLedgEntryBuf.CopyFromGenJnlLine(GenJournalLine);
        TempDtldCVLedgEntryBuf."CV Ledger Entry No." := VendLedgEntry."Entry No.";
        TempDtldCVLedgEntryBuf."Document Type" := TempDtldCVLedgEntryBuf."Document Type"::" ";
        CVLedgEntryBuf.CopyFromVendLedgEntry(VendLedgEntry);
        TempDtldCVLedgEntryBuf.InsertDtldCVLedgEntry(TempDtldCVLedgEntryBuf, CVLedgEntryBuf, TRUE);
        IF DtldVendLedgEntry.FINDLAST() THEN
            Offset := DtldVendLedgEntry."Entry No.";
        DtldVendLedgEntry.Init();
        DtldVendLedgEntry.TRANSFERFIELDS(TempDtldCVLedgEntryBuf);
        DtldVendLedgEntry."Entry No." := Offset + 1;
        DtldVendLedgEntry."Journal Batch Name" := GenJournalLine."Journal Batch Name";
        DtldVendLedgEntry."Reason Code" := GenJournalLine."Reason Code";
        DtldVendLedgEntry."Source Code" := GenJournalLine."Source Code";
        DtldVendLedgEntry."Transaction No." := NextTransactionNo;
        DtldVendLedgEntry."Initial Entry Global Dim. 1" := VendLedgEntry."Global Dimension 1 Code";
        DtldVendLedgEntry."Initial Entry Global Dim. 2" := VendLedgEntry."Global Dimension 2 Code";
        if MovimientoIRPF.TipoDocumentoIRPF = MovimientoIRPF.TipoDocumentoIRPF::Factura then
            DtldVendLedgEntry."Initial Document Type" := DtldVendLedgEntry."Initial Document Type"::Invoice
        else
            DtldVendLedgEntry."Initial Document Type" := DtldVendLedgEntry."Initial Document Type"::"Credit Memo";
        DtldVendLedgEntry."Amount (LCY)" := 0;
        DtldVendLedgEntry.Amount := 0;
        DtldVendLedgEntry."Debit Amount" := 0;
        DtldVendLedgEntry."Debit Amount (LCY)" := 0;
        DtldVendLedgEntry."Credit Amount" := 0;
        DtldVendLedgEntry."Credit Amount (LCY)" := 0;
        DtldVendLedgEntry.Amount := -GenJournalLine.ImporteIRPF;
        DtldVendLedgEntry."Amount (LCY)" := -GenJournalLine.ImporteDLIRPF;
        if DtldVendLedgEntry.Amount > 0 then begin
            DtldVendLedgEntry."Debit Amount" := ABS(GenJournalLine.ImporteIRPF);
            DtldVendLedgEntry."Debit Amount (LCY)" := ABS(GenJournalLine.ImporteDLIRPF);
        end else begin
            DtldVendLedgEntry."Credit Amount" := ABS(GenJournalLine.ImporteIRPF);
            DtldVendLedgEntry."Credit Amount (LCY)" := ABS(GenJournalLine.ImporteDLIRPF);
        end;
        DtldVendLedgEntry.INSERT(TRUE);
        //#endregion
        //#endregion            
    end;

    local procedure VendorBillsIRPF(PurchHeader: Record "Purchase Header"; var TotalPurchLine: Record "Purchase Line"; VendLedgEntry: Record "Vendor Ledger Entry")
    begin
        //Antes de hacer los efectos, le quito la parte de IRPF al total
        TotalPurchLine."Amount Including VAT" := TotalPurchLine."Amount Including VAT" - TotalPurchLine.ImporteIRPF;
    end;

    local procedure VendorBankContrapartidaIRPF(var GenJnlLine: Record "Gen. Journal Line"; var TotalPurchLineLCY: Record "Purchase Line")
    begin
        //Antes de hacer el mocimiento de contrapartida le quito el valor del IRPF
        GenJnlLine."Amount (LCY)" := GenJnlLine."Amount (LCY)" - TotalPurchLineLCY.ImporteIRPF;
        GenJnlLine.Amount := GenJnlLine.Amount - TotalPurchLineLCY.ImporteIRPF;
    end;
    //#region Liquidar movimientos de Proveedor
    local procedure ApplyVendorLedgerEntry(var PurchInvHeader: Record "Purch. Inv. Header"; var PurchCrMemoHdr: Record "Purch. Cr. Memo Hdr."; GenJnlLineDocType: Integer)
    var
        ApplyingVendLedgEntry: Record "Vendor Ledger Entry";
        GenJournalLine: Record "Gen. Journal Line";
        codDocumento: Code[20];
        datPostingDate: Date;
        codGrupoIRPF: Code[20];
        boolPositive: Boolean;
        boolContraPositive: Boolean;
    begin
        //Liquidamos los movimientos del Proveedor. Nuestro movimiento con el del sistema
        CASE GenJnlLineDocType OF
            GenJournalLine."Document Type"::Invoice:
                begin
                    codDocumento := PurchInvHeader."No.";
                    datPostingDate := PurchInvHeader."Posting Date";
                    codGrupoIRPF := PurchInvHeader.CodigoGrupoIRPF;
                    boolPositive := true;
                    boolContraPositive := false;
                end;
            GenJournalLine."Document Type"::"Credit Memo":
                begin
                    codDocumento := PurchCrMemoHdr."No.";
                    datPostingDate := PurchCrMemoHdr."Posting Date";
                    codGrupoIRPF := PurchCrMemoHdr.CodigoGrupoIRPF;
                    boolPositive := false;
                    boolContraPositive := true;
                end;
        end;
        if codGrupoIRPF = '' then
            exit;
        if ApplyVendorIRPF(codDocumento, datPostingDate, ApplyingVendLedgEntry, boolPositive) then
            ApplyVendorEntries(codDocumento, ApplyingVendLedgEntry, boolContraPositive);
    end;

    local procedure ApplyVendorIRPF(codDocumento: Code[20]; datFecha: Date; var ApplyingVendLedgEntry: Record "Vendor Ledger Entry"; boolPositive: Boolean): Boolean
    var
        DtldVendLedgEntry: Record "Detailed Vendor Ledg. Entry";
    begin
        //Buscamos nuestro movimiento y lo marcamos para despues buscar el otro movimiento y marcarlo tambien
        //#region Buscamos nuestro movimiento recien creado para marcarlo
        ApplyingVendLedgEntry.Reset();
        ApplyingVendLedgEntry.SETCURRENTKEY("Vendor No.", Open, Positive);
        ApplyingVendLedgEntry.SetRange("Document No.", codDocumento);
        ApplyingVendLedgEntry.SetRange(Open, true);
        ApplyingVendLedgEntry.SetRange(Positive, boolPositive);
        ApplyingVendLedgEntry.SetRange("Posting Date", datFecha);
        ApplyingVendLedgEntry.SetRange("Document Type", ApplyingVendLedgEntry."Document Type"::" ");
        if boolPositive then
            ApplyingVendLedgEntry.SetRange("Applies-to Doc. Type", ApplyingVendLedgEntry."Applies-to Doc. Type"::Invoice)
        else
            ApplyingVendLedgEntry.SetRange("Applies-to Doc. Type", ApplyingVendLedgEntry."Applies-to Doc. Type"::"Credit Memo");
        //#endregion
        if ApplyingVendLedgEntry.FINDFIRST() then begin
            //#region Marcamos el movimiento que hemos encontrado
            ApplyingVendLedgEntry.CALCFIELDS("Remaining Amount");
            ApplyingVendLedgEntry."Applying Entry" := TRUE;
            ApplyingVendLedgEntry."Applies-to ID" := '*****';
            ApplyingVendLedgEntry."Amount to Apply" := ApplyingVendLedgEntry."Remaining Amount";
            DtldVendLedgEntry.SETCURRENTKEY("Vendor Ledger Entry No.");
            DtldVendLedgEntry.SETRANGE("Vendor Ledger Entry No.", ApplyingVendLedgEntry."Entry No.");
            DtldVendLedgEntry.MODIFYALL("Initial Entry Due Date", ApplyingVendLedgEntry."Due Date");
            ApplyingVendLedgEntry.Modify();
            //#endregion
            exit(true);
        end;
        exit(false);
    end;

    local procedure ApplyVendorEntries(codDocumento: Code[20]; var ApplyingVendLedgEntry: Record "Vendor Ledger Entry"; boolPositive: Boolean)
    var
        VendLedgEntry: Record "Vendor Ledger Entry";
        VendEntryApplyPostedEntries: Codeunit "VendEntry-Apply Posted Entries";
        VendEntrySetApplID: Codeunit "Vend. Entry-SetAppl.ID";
        ApplicationDate: Date;
        Applied: Boolean;
    begin
        //Buscamos el movimiento del sistema para marcarlo y liquidarlo junto al movimiento nuestro que previamente hemos marcado
        //#region Buscar movimiento
        VendLedgEntry.SETCURRENTKEY("Vendor No.", Open, Positive);
        VendLedgEntry.SETRANGE("Vendor No.", ApplyingVendLedgEntry."Vendor No.");
        VendLedgEntry.SETRANGE(Open, TRUE);
        VendLedgEntry.SetRange(Positive, boolPositive);
        VendLedgEntry.SETRANGE("Document No.", codDocumento);
        //#endregion
        if VendLedgEntry.FINDFIRST() then begin
            //#region Liquidamos los 2 movimientos          
            VendEntrySetApplID.SetApplId(VendLedgEntry, ApplyingVendLedgEntry, ApplyingVendLedgEntry."Applies-to ID");
            ApplicationDate := VendEntryApplyPostedEntries.GetApplicationDate(ApplyingVendLedgEntry);
            if SingleInstanceIRPF.GetPreviewMode() = '' then
                Applied := VendEntryApplyPostedEntries.Apply(ApplyingVendLedgEntry, ApplyingVendLedgEntry."Document No.", ApplicationDate);
            SingleInstanceIRPF.SetPreviewMode('');
            //#endregion
        end;
    end;
    //#endregion
    //#region Funciones duplicadas del estandar
    local procedure GetCurrencyExchRate(GenJournalLine: Record "Gen. Journal Line")
    var
        GLSetup: Record "General Ledger Setup";
    begin
        //Funcion del Estandar duplicada aqui.
        GLSetup.GET();
        AddCurrencyCode := GLSetup."Additional Reporting Currency";
        IF AddCurrencyCode = '' THEN
            EXIT;

        AddCurrency.GET(AddCurrencyCode);
        AddCurrency.TESTFIELD("Amount Rounding Precision");
        AddCurrency.TESTFIELD("Residual Gains Account");
        AddCurrency.TESTFIELD("Residual Losses Account");

        NewCurrencyDate := GenJournalLine."Posting Date";
        IF GenJournalLine."Reversing Entry" THEN
            NewCurrencyDate := NewCurrencyDate - 1;
        IF (NewCurrencyDate <> CurrencyDate) OR UseCurrFactorOnly THEN BEGIN
            UseCurrFactorOnly := FALSE;
            CurrencyDate := NewCurrencyDate;
            CurrencyFactor := CurrExchRate.ExchangeRate(CurrencyDate, AddCurrencyCode);
        END;
        IF (GenJournalLine."FA Add.-Currency Factor" <> 0) AND (GenJournalLine."FA Add.-Currency Factor" <> CurrencyFactor) THEN BEGIN
            UseCurrFactorOnly := TRUE;
            CurrencyDate := 0D;
            CurrencyFactor := GenJournalLine."FA Add.-Currency Factor";
        END;
    end;

    local procedure GLCalcAddCurrency(Amount: Decimal; AddCurrAmount: Decimal; OldAddCurrAmount: Decimal; UseAddCurrAmount: Boolean; GenJnlLine: Record "Gen. Journal Line"): Decimal
    var
        GLSetup: Record "General Ledger Setup";
    begin
        //Funcion del Estandar duplicada aqui.
        GLSetup.GET();
        AddCurrencyCode := GLSetup."Additional Reporting Currency";
        IF (AddCurrencyCode <> '') AND (GenJnlLine."Additional-Currency Posting" = GenJnlLine."Additional-Currency Posting"::None) THEN BEGIN
            IF (GenJnlLine."Source Currency Code" = AddCurrencyCode) AND UseAddCurrAmount THEN
                EXIT(AddCurrAmount);
            EXIT(ExchangeAmtLCYToFCY2(Amount));
        END;
        EXIT(OldAddCurrAmount);
    end;

    local procedure ExchangeAmtLCYToFCY2(Amount: Decimal): Decimal
    begin
        //Funcion del Estandar duplicada aqui.        
        IF UseCurrFactorOnly THEN
            EXIT(ROUND(CurrExchRate.ExchangeAmtLCYToFCYOnlyFactor(Amount, CurrencyFactor), AddCurrency."Amount Rounding Precision"));
        EXIT(ROUND(CurrExchRate.ExchangeAmtLCYToFCY(CurrencyDate, AddCurrencyCode, Amount, CurrencyFactor), AddCurrency."Amount Rounding Precision"));
    end;
    //#endregion   
    //#endregion
    //#region Tools del Modulo
    procedure ActivePurchaseIRPF(): Boolean
    var
        PurchasesPayablesSetup: Record "Purchases & Payables Setup";
    begin
        if not PurchasesPayablesSetup.Get() then
            PurchasesPayablesSetup.Init();
        exit(PurchasesPayablesSetup.ActivoIRPF);
    end;

    procedure ShowTotalPurchasesIRPF(): Boolean
    var
        PurchasesPayablesSetup: Record "Purchases & Payables Setup";
    begin
        if not PurchasesPayablesSetup.Get() then
            PurchasesPayablesSetup.Init();
        if PurchasesPayablesSetup.ActivoIRPF = false then
            exit(false);
        exit(PurchasesPayablesSetup.MostrarTotalFacturaSinIRPF);
    end;
    //#endregion    
    //#endregion
}