codeunit 77004 "PurchaseHeaderMgtIRPF"
{
    //#region Purchase Header Mgt.
    var
        MgtIRPF: Codeunit MgtIRPF;
    //#region Eventos
    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnAfterCopyBuyFromVendorFieldsFromVendor', '', true, true)]
    local procedure OnAfterCopyBuyFromVendorFieldsFromVendor(var PurchaseHeader: Record "Purchase Header"; xPurchaseHeader: Record "Purchase Header"; Vendor: Record Vendor)
    begin
        //Despues de poner el proveedor
        if not MgtIRPF.ActivePurchaseIRPF() then begin
            PurchaseHeader.Validate(CodigoGrupoIRPF, '');
            exit;
        end;
        PurchaseHeader.Validate(CodigoGrupoIRPF, Vendor.CodigoGrupoIRPF);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnRecreatePurchLinesOnBeforeInsertPurchLine', '', true, true)]
    local procedure OnRecreatePurchLinesOnBeforeInsertPurchLine(var PurchaseLine: Record "Purchase Line"; var TempPurchaseLine: Record "Purchase Line")
    var
        PurchaseHeader: Record "Purchase Header";
    begin
        //Al recrear las lineas de compra
        if not PurchaseHeader.Get(PurchaseLine."Document Type", PurchaseLine."Document No.") then
            PurchaseHeader.Init();
        case true of
            TempPurchaseLine."Drop Shipment":
                PurchaseLine.VALIDATE("Direct Unit Cost", TempPurchaseLine."Direct Unit Cost");
            TempPurchaseLine."Special Order":
                PurchaseLine.VALIDATE("Direct Unit Cost", TempPurchaseLine."Direct Unit Cost");
            else
                PurchaseLine.VALIDATE("Direct Unit Cost", TempPurchaseLine."Direct Unit Cost");
        end;
    end;
    //#endregion
    //#endregion
}