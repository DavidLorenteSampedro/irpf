codeunit 77003 "SalesLineMgtIRPF"
{
    var
        SalesHeader: Record "Sales Header";
        Currency: Record Currency;
    //#region Eventos
    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnAfterValidateEvent', 'No.', true, true)]
    local procedure OnAfterValidateEventNo(var Rec: Record "Sales Line"; var xRec: Record "Sales Line"; CurrFieldNo: Integer)
    begin
        //Despues de validar 
        TraeIRPF(Rec, xRec);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnAfterValidateEvent', 'CodigoGrupoIRPF', true, true)]
    procedure OnAfterValidateEventCodigoGrupoIRPF(var Rec: Record "Sales Line"; var xRec: Record "Sales Line"; CurrFieldNo: Integer)
    var
        Customer: Record Customer;
        GruposIRPF: Record GruposIRPF;
    begin
        if (Rec.CodigoGrupoIRPF <> '') and (Rec.Type IN [Rec.Type::"G/L Account", Rec.Type::"Fixed Asset"]) then begin
            GetSalesHeader(Rec);
            Customer.GET(SalesHeader."Sell-to Customer No.");
            if Customer.CodigoGrupoIRPF = '' then
                Error('No se puede calcular IRPF si el cliente no tiene configurado IRPF en su ficha.');

            if GruposIRPF.GET(Rec.CodigoGrupoIRPF) then begin
                Rec.TipoCalculoIRPF := GruposIRPF.TipoCalculoIRPF;
                Rec.PorcentajeRetencionIRPF := GruposIRPF.PorcentajeRetencionIRPF;
                Rec.Validate(BaseIRPF);
            end;
        end else begin
            Rec.CodigoGrupoIRPF := '';
            Rec.TipoCalculoIRPF := Rec.TipoCalculoIRPF::Blank;
            Rec.ImporteIRPF := 0;
            Rec.PorcentajeRetencionIRPF := 0;
            Rec.BaseIRPF := 0;
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnAfterUpdateVATAmounts', '', true, true)]
    procedure OnAfterUpdateVATAmounts(var SalesLine: Record "Sales Line")
    begin
        IF SalesLine.CodigoGrupoIRPF <> '' THEN begin
            SalesLine.VALIDATE(BaseIRPF);
            SalesLine.Modify();
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnAfterValidateEvent', 'ImporteIRPF', true, true)]
    procedure OnAfterValidateEventImporteIRPF(var Rec: Record "Sales Line"; var xRec: Record "Sales Line"; CurrFieldNo: Integer)
    begin
        GetSalesHeader(Rec);
        Rec.ImporteIRPF := ROUND(Rec.BaseIRPF * Rec.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnAfterValidateEvent', 'BaseIRPF', true, true)]
    procedure OnAfterValidateEventBaseIRPF(var Rec: Record "Sales Line"; var xRec: Record "Sales Line"; CurrFieldNo: Integer)
    begin
        GetSalesHeader(Rec);
        if Rec.TipoCalculoIRPF = Rec.TipoCalculoIRPF::Importe then begin
            if SalesHeader."Prices Including VAT" then
                Rec.BaseIRPF := ROUND((Rec."Line Amount" - Rec."Inv. Discount Amount") / (1 + ((Rec."VAT %" + Rec."EC %") / 100)), Currency."Amount Rounding Precision")
            else
                Rec.BaseIRPF := Rec."Line Amount" - Rec."Inv. Discount Amount";
            Rec.VALIDATE(ImporteIRPF);
        end else begin
            if Rec.TipoCalculoIRPF = Rec.TipoCalculoIRPF::ImporteIVAIncluido then
                if NOT SalesHeader."Prices Including VAT" then
                    Rec.BaseIRPF := ROUND((Rec."Line Amount" - Rec."Inv. Discount Amount") * (1 + ((Rec."VAT %" + Rec."EC %") / 100)), Currency."Amount Rounding Precision")
                else
                    Rec.BaseIRPF := Rec."Line Amount" - Rec."Inv. Discount Amount";
            Rec.VALIDATE(ImporteIRPF);
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnAfterCalcVATAmountLines', '', true, true)]
    procedure OnAfterCalcVATAmountLines(var SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line"; var VATAmountLine: Record "VAT Amount Line"; QtyType: Option)
    begin
        //General,Invoicing,Shipping      
        SalesLine.SetRange("Document Type", SalesHeader."Document Type");
        SalesLine.SetRange("Document No.", SalesHeader."No.");
        if SalesLine.FindSet() then
            repeat
                if VATAmountLine.GET(SalesLine."VAT Identifier", SalesLine."VAT Calculation Type", SalesLine."Tax Group Code", FALSE, SalesLine."Line Amount" >= 0) then begin
                    VATAmountLine.CodigoGrupoIRPF := SalesLine.CodigoGrupoIRPF;
                    VATAmountLine.TipoCalculoIRPF := SalesLine.TipoCalculoIRPF;
                    VATAmountLine.PorcentajeRetencionIRPF := SalesLine.PorcentajeRetencionIRPF;
                    VATAmountLine.Modify();
                end;
            until SalesLine.Next() = 0;
        UpdateVatAmountLine(SalesHeader, SalesLine, VATAmountLine);
    end;
    //#endregion
    //#region Funciones
    procedure TraeIRPF(var Rec: Record "Sales Line"; var xRec: Record "Sales Line")
    var
        GenProdPostingGrp: Record "Gen. Product Posting Group";
    begin
        IF NOT (Rec.Type IN [Rec.Type::"G/L Account", Rec.Type::"Fixed Asset"]) THEN EXIT;
        GetSalesHeader(Rec);
        IF SalesHeader.CodigoGrupoIRPF <> '' THEN
            IF GenProdPostingGrp.GET(Rec."Gen. Prod. Posting Group") THEN
                IF GenProdPostingGrp.SoportaIRPF THEN
                    Rec.VALIDATE(CodigoGrupoIRPF, SalesHeader.CodigoGrupoIRPF);
    end;

    procedure UpdateVatAmountLine(var SalesHeaderLocal: Record "Sales Header"; var SalesLine: Record "Sales Line"; var VATAmountLine: Record "VAT Amount Line")
    var
        PrevVatAmountLine: Record "VAT Amount Line";
    begin
        if not VATAmountLine.FindSet() then
            exit;
        GetSalesHeader(SalesLine);
        repeat
            if (PrevVatAmountLine."VAT Identifier" <> VATAmountLine."VAT Identifier") OR (PrevVatAmountLine."VAT Calculation Type" <> VATAmountLine."VAT Calculation Type") OR (PrevVatAmountLine."Tax Group Code" <> VATAmountLine."Tax Group Code") OR (PrevVatAmountLine."Use Tax" <> VATAmountLine."Use Tax") then
                PrevVatAmountLine.Init();
            if SalesHeaderLocal."Prices Including VAT" AND NOT (VATAmountLine."VAT %" = 0) then begin
                case VATAmountLine."VAT Calculation Type" of
                    VATAmountLine."VAT Calculation Type"::"Normal VAT", VATAmountLine."VAT Calculation Type"::"No taxable VAT":
                        begin
                            if VATAmountLine.TipoCalculoIRPF = VATAmountLine.TipoCalculoIRPF::Importe then begin
                                if SalesHeaderLocal."Prices Including VAT" then
                                    VATAmountLine.BaseIRPF := ROUND((VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount") / (1 + ((VATAmountLine."VAT %" + VATAmountLine."EC %") / 100)), Currency."Amount Rounding Precision")
                                else
                                    VATAmountLine.BaseIRPF := VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount";
                                VATAmountLine.ImporteIRPF := ROUND(VATAmountLine.BaseIRPF * VATAmountLine.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
                            end else begin
                                if VATAmountLine.TipoCalculoIRPF = VATAmountLine.TipoCalculoIRPF::ImporteIVAIncluido then
                                    if NOT SalesHeaderLocal."Prices Including VAT" then
                                        VATAmountLine.BaseIRPF := ROUND((VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount") * (1 + ((VATAmountLine."VAT %" + VATAmountLine."EC %") / 100)), Currency."Amount Rounding Precision")
                                    else
                                        VATAmountLine.BaseIRPF := VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount";
                                VATAmountLine.ImporteIRPF := ROUND(VATAmountLine.BaseIRPF * VATAmountLine.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
                            end;
                        end;
                end;
            end else
                case VATAmountLine."VAT Calculation Type" of
                    VATAmountLine."VAT Calculation Type"::"No taxable VAT", VATAmountLine."VAT Calculation Type"::"Normal VAT":
                        begin
                            IF VATAmountLine.TipoCalculoIRPF = VATAmountLine.TipoCalculoIRPF::Importe THEN BEGIN
                                IF SalesHeaderLocal."Prices Including VAT" THEN
                                    VATAmountLine.BaseIRPF := ROUND((VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount") / (1 + ((VATAmountLine."VAT %" + VATAmountLine."EC %") / 100)), Currency."Amount Rounding Precision")
                                ELSE
                                    VATAmountLine.BaseIRPF := VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount";
                                VATAmountLine.ImporteIRPF := ROUND(VATAmountLine.BaseIRPF * VATAmountLine.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
                            END ELSE BEGIN
                                IF VATAmountLine.TipoCalculoIRPF = VATAmountLine.TipoCalculoIRPF::ImporteIVAIncluido THEN
                                    IF NOT SalesHeaderLocal."Prices Including VAT" THEN
                                        VATAmountLine.BaseIRPF := ROUND((VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount") * (1 + ((VATAmountLine."VAT %" + VATAmountLine."EC %") / 100)), Currency."Amount Rounding Precision")
                                    ELSE
                                        VATAmountLine.BaseIRPF := VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount";
                                VATAmountLine.ImporteIRPF := ROUND(VATAmountLine.BaseIRPF * VATAmountLine.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
                            END;
                        END;
                END;
            VATAmountLine.TotalImporteSinIRPF := VATAmountLine."Amount Including VAT" - VATAmountLine.ImporteIRPF;
            VATAmountLine.MODIFY();
        UNTIL VATAmountLine.Next() = 0;
    end;

    procedure GetSalesHeader(var Rec: Record "Sales Line")
    begin
        Rec.TestField("Document No.");
        SalesHeader.GET(Rec."Document Type", Rec."Document No.");
        if SalesHeader."Currency Code" = '' then
            Currency.InitRoundingPrecision()
        else begin
            SalesHeader.TESTFIELD("Currency Factor");
            Currency.GET(SalesHeader."Currency Code");
            Currency.TESTFIELD("Amount Rounding Precision");
        end;
    end;
    //#endregion
}