codeunit 77008 "SingleInstanceIRPF"
{
    SingleInstance = true;
    procedure SetPreviewMode(SetPreview: Code[20])
    begin
        PreviewMode := SetPreview;
    end;

    procedure GetPreviewMode(): Code[20]
    begin
        exit(PreviewMode);
    end;

    var
        PreviewMode: Code[20];
}