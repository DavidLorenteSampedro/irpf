codeunit 77002 "SalesHeaderMgtIRPF"
{
    //#region Eventos
    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnAfterCopySellToCustomerAddressFieldsFromCustomer', '', true, true)]
    procedure OnAfterCopySellToCustomerAddressFieldsFromCustomer(var SalesHeader: Record "Sales Header"; SellToCustomer: Record Customer; CurrentFieldNo: Integer)
    begin
        SalesHeader.CodigoGrupoIRPF := SellToCustomer.CodigoGrupoIRPF;
    end;
    //#endregion

}