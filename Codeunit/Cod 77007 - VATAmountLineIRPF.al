codeunit 77007 "VATAmountLineIRPF"
{
    //#region VAT Amount Line
    var
        PurchaseHeader: Record "Purchase Header";
        Currency: Record Currency;
    //#region Eventos    
    [EventSubscriber(ObjectType::Table, Database::"VAT Amount Line", 'OnAfterCopyFromPurchCrMemoLine', '', true, true)]
    local procedure OnAfterCopyFromPurchCrMemoLine(PurchCrMemoLine: Record "Purch. Cr. Memo Line"; var VATAmountLine: Record "VAT Amount Line")
    var
        PurchCrMemoHdr: Record "Purch. Cr. Memo Hdr.";
    begin
        //Calculamos los totales para las estadisticas 
        if not PurchCrMemoHdr.Get(PurchCrMemoLine."Document No.") then
            exit;
        PurchCrMemoHdr.CalcFields(ImporteIRPF, BaseIRPF);
        VATAmountLine.ImporteIRPF := PurchCrMemoHdr.ImporteIRPF;
        VATAmountLine.BaseIRPF := PurchCrMemoHdr.BaseIRPF;
        VATAmountLine.CodigoGrupoIRPF := PurchCrMemoHdr.CodigoGrupoIRPF;
        VATAmountLine.PorcentajeRetencionIRPF := PurchCrMemoHdr.PorcentajeIRPF;
        VATAmountLine.TipoCalculoIRPF := PurchCrMemoHdr.TipoCalculoIRPF;
        VATAmountLine.TotalImporteSinIRPF := VATAmountLine."Amount Including VAT" - VATAmountLine.ImporteIRPF;
    end;

    [EventSubscriber(ObjectType::Table, Database::"VAT Amount Line", 'OnAfterCopyFromPurchInvLine', '', true, true)]
    local procedure OnAfterCopyFromPurchInvLine(PurchInvLine: Record "Purch. Inv. Line"; var VATAmountLine: Record "VAT Amount Line")
    var
        PuchInvHeader: Record "Purch. Inv. Header";
    begin
        //Calculamos los totales para las estadisticas 
        if not PuchInvHeader.Get(PurchInvLine."Document No.") then
            exit;
        PuchInvHeader.CalcFields(ImporteIRPF, BaseIRPF);
        VATAmountLine.ImporteIRPF := PuchInvHeader.ImporteIRPF;
        VATAmountLine.BaseIRPF := PuchInvHeader.BaseIRPF;
        VATAmountLine.CodigoGrupoIRPF := PuchInvHeader.CodigoGrupoIRPF;
        VATAmountLine.PorcentajeRetencionIRPF := PuchInvHeader.PorcentajeIRPF;
        VATAmountLine.TipoCalculoIRPF := PuchInvHeader.TipoCalculoIRPF;
        VATAmountLine.TotalImporteSinIRPF := VATAmountLine."Amount Including VAT" - VATAmountLine.ImporteIRPF;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterCalcVATAmountLines', '', true, true)]
    local procedure OnAfterCalcVATAmountLines(var PurchHeader: Record "Purchase Header"; var PurchLine: Record "Purchase Line"; var VATAmountLine: Record "VAT Amount Line"; QtyType: Option)
    begin
        //Despues de calcular la linea de iva de la linea de compra   
        PurchLine.SetRange("Document Type", PurchHeader."Document Type");
        PurchLine.SetRange("Document No.", PurchHeader."No.");
        if PurchLine.FindSet() then
            repeat
                if VATAmountLine.GET(PurchLine."VAT Identifier", PurchLine."VAT Calculation Type", PurchLine."Tax Group Code", FALSE, PurchLine."Line Amount" >= 0) then begin
                    VATAmountLine.CodigoGrupoIRPF := PurchLine.CodigoGrupoIRPF;
                    VATAmountLine.TipoCalculoIRPF := PurchLine.TipoCalculoIRPF;
                    VATAmountLine.PorcentajeRetencionIRPF := PurchLine.PorcentajeRetencionIRPF;
                    VATAmountLine.Modify();
                end;
            until PurchLine.Next() = 0;
        UpdateVatAmountLine(PurchHeader, PurchLine, VATAmountLine);
    end;

    [EventSubscriber(ObjectType::Table, Database::"VAT Amount Line", 'OnAfterValidateEvent', 'Invoice Discount Amount', true, true)]
    local procedure OnAfterValidateEventInvoiceDiscountAmount(var Rec: Record "VAT Amount Line"; var xRec: Record "VAT Amount Line")
    begin
        //Al actualizar las líneas recalculamos el IRPF, esto se utiliza en las estadisticas sobre todo
        if (xRec."VAT Identifier" <> Rec."VAT Identifier") OR (xRec."VAT Calculation Type" <> Rec."VAT Calculation Type") OR (xRec."Tax Group Code" <> Rec."Tax Group Code") OR (xRec."Use Tax" <> Rec."Use Tax") then
            xRec.Init();
        if Rec."Prices Including VAT" AND NOT (Rec."VAT %" = 0) then begin
            case Rec."VAT Calculation Type" of
                Rec."VAT Calculation Type"::"Normal VAT", Rec."VAT Calculation Type"::"No taxable VAT":
                    begin
                        if Rec.TipoCalculoIRPF = Rec.TipoCalculoIRPF::Importe then begin
                            if Rec."Prices Including VAT" then
                                Rec.BaseIRPF := ROUND((Rec."Line Amount" - Rec."Invoice Discount Amount") / (1 + ((Rec."VAT %" + Rec."EC %") / 100)), Currency."Amount Rounding Precision")
                            else
                                Rec.BaseIRPF := Rec."Line Amount" - Rec."Invoice Discount Amount";
                            Rec.ImporteIRPF := ROUND(Rec.BaseIRPF * Rec.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
                        end else begin
                            if Rec.TipoCalculoIRPF = Rec.TipoCalculoIRPF::ImporteIVAIncluido then
                                if NOT Rec."Prices Including VAT" then
                                    Rec.BaseIRPF := ROUND((Rec."Line Amount" - Rec."Invoice Discount Amount") * (1 + ((Rec."VAT %" + Rec."EC %") / 100)), Currency."Amount Rounding Precision")
                                else
                                    Rec.BaseIRPF := Rec."Line Amount" - Rec."Invoice Discount Amount";
                            Rec.ImporteIRPF := ROUND(Rec.BaseIRPF * Rec.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
                        end;
                    end;
            end;
        end else
            case Rec."VAT Calculation Type" of
                Rec."VAT Calculation Type"::"No taxable VAT", Rec."VAT Calculation Type"::"Normal VAT":
                    begin
                        IF Rec.TipoCalculoIRPF = Rec.TipoCalculoIRPF::Importe THEN BEGIN
                            IF Rec."Prices Including VAT" THEN
                                Rec.BaseIRPF := ROUND((Rec."Line Amount" - Rec."Invoice Discount Amount") / (1 + ((Rec."VAT %" + Rec."EC %") / 100)), Currency."Amount Rounding Precision")
                            ELSE
                                Rec.BaseIRPF := Rec."Line Amount" - Rec."Invoice Discount Amount";
                            Rec.ImporteIRPF := ROUND(Rec.BaseIRPF * Rec.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
                        END ELSE BEGIN
                            IF Rec.TipoCalculoIRPF = Rec.TipoCalculoIRPF::ImporteIVAIncluido THEN
                                IF NOT Rec."Prices Including VAT" THEN
                                    Rec.BaseIRPF := ROUND((Rec."Line Amount" - Rec."Invoice Discount Amount") * (1 + ((Rec."VAT %" + Rec."EC %") / 100)), Currency."Amount Rounding Precision")
                                ELSE
                                    Rec.BaseIRPF := Rec."Line Amount" - Rec."Invoice Discount Amount";
                            Rec.ImporteIRPF := ROUND(Rec.BaseIRPF * Rec.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
                        END;
                    END;
            END;
        Rec.TotalImporteSinIRPF := Rec."Amount Including VAT" - Rec.ImporteIRPF;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterUpdateVATAmounts', '', true, true)]
    local procedure OnAfterUpdateVATAmounts(var PurchaseLine: Record "Purchase Line")
    begin
        //Despues de modificar el UpdtaVatAmount
        IF PurchaseLine.CodigoGrupoIRPF <> '' THEN begin
            PurchaseLine.VALIDATE(BaseIRPF);
        end;
    end;
    //#endregion
    //#region Funciones
    local procedure UpdateVatAmountLine(var PurchaseHeaderLocal: Record "Purchase Header"; var PurchaseLine: Record "Purchase Line"; var VATAmountLine: Record "VAT Amount Line")
    var
        PrevVatAmountLine: Record "VAT Amount Line";
    begin
        //Actualizamos las lineas de iva de las lineas de compra
        if not VATAmountLine.FindSet() then
            exit;
        GetPurchaseHeader(PurchaseLine);
        repeat
            if (PrevVatAmountLine."VAT Identifier" <> VATAmountLine."VAT Identifier") OR (PrevVatAmountLine."VAT Calculation Type" <> VATAmountLine."VAT Calculation Type") OR (PrevVatAmountLine."Tax Group Code" <> VATAmountLine."Tax Group Code") OR (PrevVatAmountLine."Use Tax" <> VATAmountLine."Use Tax") then
                PrevVatAmountLine.Init();
            if PurchaseHeaderLocal."Prices Including VAT" AND NOT (VATAmountLine."VAT %" = 0) then begin
                case VATAmountLine."VAT Calculation Type" of
                    VATAmountLine."VAT Calculation Type"::"Normal VAT", VATAmountLine."VAT Calculation Type"::"No taxable VAT":
                        begin
                            if VATAmountLine.TipoCalculoIRPF = VATAmountLine.TipoCalculoIRPF::Importe then begin
                                if PurchaseHeaderLocal."Prices Including VAT" then
                                    VATAmountLine.BaseIRPF := ROUND((VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount") / (1 + ((VATAmountLine."VAT %" + VATAmountLine."EC %") / 100)), Currency."Amount Rounding Precision")
                                else
                                    VATAmountLine.BaseIRPF := VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount";
                                VATAmountLine.ImporteIRPF := ROUND(VATAmountLine.BaseIRPF * VATAmountLine.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
                            end else begin
                                if VATAmountLine.TipoCalculoIRPF = VATAmountLine.TipoCalculoIRPF::ImporteIVAIncluido then
                                    if NOT PurchaseHeaderLocal."Prices Including VAT" then
                                        VATAmountLine.BaseIRPF := ROUND((VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount") * (1 + ((VATAmountLine."VAT %" + VATAmountLine."EC %") / 100)), Currency."Amount Rounding Precision")
                                    else
                                        VATAmountLine.BaseIRPF := VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount";
                                VATAmountLine.ImporteIRPF := ROUND(VATAmountLine.BaseIRPF * VATAmountLine.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
                            end;
                        end;
                end;
            end else
                case VATAmountLine."VAT Calculation Type" of
                    VATAmountLine."VAT Calculation Type"::"No taxable VAT", VATAmountLine."VAT Calculation Type"::"Normal VAT":
                        begin
                            IF VATAmountLine.TipoCalculoIRPF = VATAmountLine.TipoCalculoIRPF::Importe THEN BEGIN
                                IF PurchaseHeaderLocal."Prices Including VAT" THEN
                                    VATAmountLine.BaseIRPF := ROUND((VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount") / (1 + ((VATAmountLine."VAT %" + VATAmountLine."EC %") / 100)), Currency."Amount Rounding Precision")
                                ELSE
                                    VATAmountLine.BaseIRPF := VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount";
                                VATAmountLine.ImporteIRPF := ROUND(VATAmountLine.BaseIRPF * VATAmountLine.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
                            END ELSE BEGIN
                                IF VATAmountLine.TipoCalculoIRPF = VATAmountLine.TipoCalculoIRPF::ImporteIVAIncluido THEN
                                    IF NOT PurchaseHeaderLocal."Prices Including VAT" THEN
                                        VATAmountLine.BaseIRPF := ROUND((VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount") * (1 + ((VATAmountLine."VAT %" + VATAmountLine."EC %") / 100)), Currency."Amount Rounding Precision")
                                    ELSE
                                        VATAmountLine.BaseIRPF := VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount";
                                VATAmountLine.ImporteIRPF := ROUND(VATAmountLine.BaseIRPF * VATAmountLine.PorcentajeRetencionIRPF / 100, Currency."Amount Rounding Precision");
                            END;
                        END;
                END;
            VATAmountLine.TotalImporteSinIRPF := VATAmountLine."Amount Including VAT" - VATAmountLine.ImporteIRPF;
            VATAmountLine.MODIFY();
        UNTIL VATAmountLine.Next() = 0;
    end;
    //#region Herramientas comunes
    local procedure GetPurchaseHeader(var Rec: Record "Purchase Line")
    begin
        Rec.TestField("Document No.");
        PurchaseHeader.GET(Rec."Document Type", Rec."Document No.");
        if PurchaseHeader."Currency Code" = '' then
            Currency.InitRoundingPrecision()
        else begin
            PurchaseHeader.TESTFIELD("Currency Factor");
            Currency.GET(PurchaseHeader."Currency Code");
            Currency.TESTFIELD("Amount Rounding Precision");
        end;
    end;
    //#endregion
    //#endregion
    //#endregion
}