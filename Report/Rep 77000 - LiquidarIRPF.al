report 77000 "LiquidarIRPF"
{
    Caption = 'Liquidar Movimientos IRPF', Comment = 'ESP="Liquidar Movimientos IRPF"';
    UsageCategory = Administration;
    ApplicationArea = All;
    ProcessingOnly = true;
    dataset
    {
        dataitem(GruposIRPF; GruposIRPF)
        {
            DataItemTableView = sorting(CodigoIRPF);
            trigger OnPreDataItem()
            begin
                CLEAR(GenJournalLine);
                GenJournalLine.Reset();
                //Borrar movimientos previos
                //GenJournalLine.Reset();
                //GenJournalLine.SetRange("Journal Batch Name", PurchasesPayablesSetup.SeccionDiarioIRPF);
                //GenJournalLine.SetRange("Journal Template Name", PurchasesPayablesSetup.LibroDiarioIRPF);

                decImporteTotal := 0;
            end;

            trigger OnAfterGetRecord()
            begin
                GetGrupoIRPF();
            end;

            trigger OnPostDataItem()
            var
                GenJournalBatch: Record "Gen. Journal Batch";
            begin
                if decImporteTotal = 0 then
                    exit;
                SetJournalTotal(decImporteTotal);
                GenJournalBatch.Reset();
                GenJournalBatch.SetRange("Journal Template Name", PurchasesPayablesSetup.LibroDiarioIRPF);
                GenJournalBatch.SetRange(Name, PurchasesPayablesSetup.SeccionDiarioIRPF);

                if GenJournalBatch.FindSet() then
                    TemplateSelectionFromBatch(GenJournalBatch);
            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    Caption = 'Parámetros', Comment = 'ESP="Parámetros"';
                    field(DesdeFecha; datDesdeFecha)
                    {
                        ApplicationArea = All;
                        Caption = 'Fecha Desde', Comment = 'ESP="Fecha Desde"';
                    }
                    field(HastaFecha; datHastaFecha)
                    {
                        ApplicationArea = All;
                        Caption = 'Fecha Hasta', Comment = 'ESP="Fecha Hasta"';
                    }
                    field(FechaCierre; datFechaCierre)
                    {
                        ApplicationArea = All;
                        Caption = 'Fecha Cierre', Comment = 'ESP="Fecha Cierre"';
                        trigger OnValidate()
                        begin
                            IF datDesdeFecha = 0D THEN
                                datDesdeFecha := datHastaFecha;
                        end;
                    }
                    field(NumeroDocumento; codNumeroDocumento)
                    {
                        ApplicationArea = All;
                        Caption = 'Nº Documento', Comment = 'ESP="Nº Documento"';
                    }
                    field(TipoContrapartida; enumTipoContrapartida)
                    {
                        ApplicationArea = All;
                        Caption = 'Contrapartida', Comment = 'ESP="Contrapartida"';
                    }
                    field(NumeroCuenta; codNumeroCuenta)
                    {
                        ApplicationArea = All;
                        Caption = 'Cuenta', Comment = 'ESP="Cuenta"';
                        trigger OnLookup(var Text: Text): Boolean
                        var
                            BankAccount: Record "Bank Account";
                            GLAccount: Record "G/L Account";
                            BankAccountList: Page "Bank Account List";
                            ChartofAccounts: Page "Chart of Accounts";
                        begin
                            case enumTipoContrapartida of
                                enumTipoContrapartida::Banco:
                                    begin
                                        CLEAR(BankAccountList);
                                        BankAccountList.LookupMode := TRUE;
                                        IF BankAccountList.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                                            BankAccountList.GETRECORD(BankAccount);
                                            codNumeroCuenta := BankAccount."No.";
                                        END;
                                    end;
                                enumTipoContrapartida::Cuenta:
                                    begin
                                        CLEAR(ChartofAccounts);
                                        ChartofAccounts.LOOKUPMODE := TRUE;
                                        IF ChartofAccounts.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                                            ChartofAccounts.GETRECORD(GLAccount);
                                            codNumeroCuenta := GLAccount."No.";
                                        END;
                                    END;
                            end;
                        end;
                    }
                }
            }
        }

    }
    trigger OnPreReport()
    begin
        if enumTipoContrapartida = enumTipoContrapartida::Blank then
            if not Confirm('¿Desea marcar las líneas como liquidadas sin realizar el asiento de liquidacion?') then
                error('Proceso detenido por el usuario');
        PurchasesPayablesSetup.Get();
        if (PurchasesPayablesSetup.LibroDiarioIRPF = '') or (PurchasesPayablesSetup.SeccionDiarioIRPF = '') then
            Error(text006Msg);
        IF (datDesdeFecha = 0D) OR (datHastaFecha = 0D) THEN
            ERROR(text004Msg)
        ELSE
            IF datHastaFecha < datDesdeFecha THEN
                ERROR(text003Msg);

        IF datFechaCierre = 0D THEN
            ERROR(text002Msg);

        IF ((enumTipoContrapartida = enumTipoContrapartida::Banco) OR (enumTipoContrapartida = enumTipoContrapartida::Cuenta)) THEN BEGIN
            IF codNumeroDocumento = '' THEN
                ERROR(text005Msg);
            IF codNumeroCuenta = '' THEN
                ERROR(text001Msg);
        END;
    end;

    local procedure GetGrupoIRPF()
    begin
        decCompras := 0;
        decVentas := 0;
        if enumTipoContrapartida = enumTipoContrapartida::Blank then
            SetLiquidado()
        else begin
            GlobalGuid := CreateGuid();
            GetAmounts();
            SetJournalLine(decVentas, GruposIRPF.CuentaRetencionRepercutidoIRPF);
            SetJournalLine(decCompras, GruposIRPF.CuentaRetencionSoportadoIRPF);
            decImporteTotal := decImporteTotal + decCompras + decVentas;
        end;
    end;

    local procedure SetLiquidado()
    var
        MovimientosIRPF: Record MovimientosIRPF;
        MovimientosIRPFModify: Record MovimientosIRPF;
    begin
        MovimientosIRPF.Reset();
        MovimientosIRPF.SETRANGE(FechaRegistroIRPF, datDesdeFecha, datHastaFecha);
        MovimientosIRPF.SETRANGE(GrupoIRPF, GruposIRPF.CodigoIRPF);
        MovimientosIRPF.SETRANGE(PendienteIRPF, TRUE);
        IF MovimientosIRPF.FindSet() THEN
            repeat
                MovimientosIRPFModify := MovimientosIRPF;
                MovimientosIRPFModify.PendienteIRPF := false;
                MovimientosIRPFModify.FechaLiquidacionIRPF := datFechaCierre;
                MovimientosIRPFModify.Modify();
            until MovimientosIRPF.Next() = 0;
    end;

    local procedure GetAmounts()
    var
        MovimientosIRPF: Record MovimientosIRPF;
        MovimientosIRPFModify: Record MovimientosIRPF;
    begin
        MovimientosIRPF.Reset();
        MovimientosIRPF.SETRANGE(FechaRegistroIRPF, datDesdeFecha, datHastaFecha);
        MovimientosIRPF.SETRANGE(GrupoIRPF, GruposIRPF.CodigoIRPF);
        MovimientosIRPF.SETRANGE(PendienteIRPF, TRUE);
        IF MovimientosIRPF.FindSet() THEN
            repeat
                if MovimientosIRPF.TipoIRPF = MovimientosIRPF.TipoIRPF::Compras then
                    decCompras := decCompras + MovimientosIRPF.ImporteIRPF
                else
                    decVentas := decVentas + MovimientosIRPF.ImporteIRPF;
                MovimientosIRPFModify := MovimientosIRPF;
                MovimientosIRPFModify.GuidMovimientoIRPF := GlobalGuid;
                MovimientosIRPFModify.Modify();
            until MovimientosIRPF.Next() = 0;
    end;

    local procedure SetJournalLine(decAmount: decimal; codAccount: Code[20])
    var
        intLinea: Integer;
    begin
        if decAmount = 0 then
            exit;
        GenJournalLine.Reset();
        GenJournalLine.SetRange("Journal Batch Name", PurchasesPayablesSetup.SeccionDiarioIRPF);
        GenJournalLine.SetRange("Journal Template Name", PurchasesPayablesSetup.LibroDiarioIRPF);
        if GenJournalLine.FindLast() then
            intLinea := GenJournalLine."Line No.";
        intLinea := intLinea + 10000;
        GenJournalLine.Reset();
        GenJournalLine.Init();
        GenJournalLine."Journal Batch Name" := PurchasesPayablesSetup.SeccionDiarioIRPF;
        GenJournalLine."Journal Template Name" := PurchasesPayablesSetup.LibroDiarioIRPF;
        GenJournalLine."Line No." := intLinea;
        GenJournalLine."Account Type" := GenJournalLine."Account Type"::"G/L Account";
        GenJournalLine.VALIDATE("Account No.", codAccount);
        GenJournalLine.VALIDATE(Amount, -decAmount);
        GenJournalLine.VALIDATE("Posting Date", datFechaCierre);
        GenJournalLine.Description := 'Liquidación de IRPF desde ' + FORMAT(datDesdeFecha) + ' hasta ' + FORMAT(datHastaFecha);
        GenJournalLine."Document No." := codNumeroDocumento;
        GenJournalLine.GuidMovimientoIRPF := GlobalGuid;
        GenJournalLine.Insert();
    end;

    local procedure SetJournalTotal(decAmount: decimal)
    var
        intLinea: Integer;
    begin
        if decAmount = 0 then
            exit;
        GenJournalLine.Reset();
        GenJournalLine.SetRange("Journal Batch Name", PurchasesPayablesSetup.SeccionDiarioIRPF);
        GenJournalLine.SetRange("Journal Template Name", PurchasesPayablesSetup.LibroDiarioIRPF);
        if GenJournalLine.FindLast() then
            intLinea := GenJournalLine."Line No.";
        intLinea := intLinea + 10000;
        GenJournalLine.Reset();
        GenJournalLine.Init();
        GenJournalLine."Journal Batch Name" := PurchasesPayablesSetup.SeccionDiarioIRPF;
        GenJournalLine."Journal Template Name" := PurchasesPayablesSetup.LibroDiarioIRPF;
        GenJournalLine."Line No." := intLinea;
        IF (enumTipoContrapartida = enumTipoContrapartida::Cuenta) THEN BEGIN
            GenJournalLine.VALIDATE("Account Type", GenJournalLine."Account Type"::"G/L Account")
        END ELSE
            GenJournalLine.VALIDATE("Account Type", GenJournalLine."Account Type"::"Bank Account");
        GenJournalLine."Account No." := codNumeroCuenta;
        GenJournalLine.VALIDATE("Posting Date", datFechaCierre);
        GenJournalLine.Description := 'Liquidación de IRPF desde ' + FORMAT(datDesdeFecha) + ' hasta ' + FORMAT(datHastaFecha);
        GenJournalLine."Document No." := codNumeroDocumento;
        GenJournalLine.VALIDATE(Amount, decAmount);
        GenJournalLine.GuidMovimientoIRPF := GlobalGuid;
        GenJournalLine.Insert();
    end;

    local procedure TemplateSelectionFromBatch(var GenJnlBatch: Record "Gen. Journal Batch")
    var
        GenJnlLine: Record "Gen. Journal Line";
        GenJnlTemplate: Record "Gen. Journal Template";
    begin

        GenJnlTemplate.Get(GenJnlBatch."Journal Template Name");
        GenJnlTemplate.TestField("Page ID");
        GenJnlBatch.TestField(Name);

        GenJnlLine.FilterGroup := 2;
        GenJnlLine.SetRange("Journal Template Name", GenJnlTemplate.Name);
        GenJnlLine.SetRange("Document No.", codNumeroDocumento);
        GenJnlLine.FilterGroup := 0;

        GenJnlLine."Journal Template Name" := '';
        GenJnlLine."Journal Batch Name" := GenJnlBatch.Name;
        PAGE.Run(GenJnlTemplate."Page ID", GenJnlLine);
    end;

    var
        PurchasesPayablesSetup: Record "Purchases & Payables Setup";
        GenJournalLine: Record "Gen. Journal Line";
        codNumeroCuenta: Code[20];
        codNumeroDocumento: Code[20];
        datFechaCierre: Date;
        datHastaFecha: Date;
        datDesdeFecha: Date;
        decImporteTotal: Decimal;
        enumTipoContrapartida: Enum TipoContrapartidaIRPF;
        text001Msg: Label 'Debe introducir el numero de cuenta';
        text002Msg: Label 'Debe introducir fecha de cierre';

        text003Msg: Label 'La fecha hasta debe ser  mayor que la fecha desde';
        text004Msg: Label 'Debe indicar fecha desde y fecha hasta';
        text005Msg: Label 'Debe introducir Nº documento';
        text006Msg: Label 'No hay configurado nngún libro o sección para registrar la liquidación';
        decCompras: Decimal;
        decVentas: Decimal;
        GlobalGuid: Guid;

}